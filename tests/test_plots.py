# created by Subhas C Bera

import numpy as np
import matplotlib.pyplot as plt
import time

from Change_point_analysis.module.functions import *

x = np.arange(100)
y = np.sin(x*0.3)
fig = plt.figure(tight_layout=True)

t0 = time.time()

axs = fig.subplots(2,1, sharex=True, gridspec_kw = {'wspace':0, 'hspace':0})
ax = axs[0]
ax1 = axs[1]
create_one_ax_plot(ax, x, y, ylabel='y1')
# creat_one_ax_plot(ax, x+3, y, ylabel='y1')
create_one_ax_plot(ax1, x+10, y, xlabel='x', ylabel='y2')
# plt.close(fig)

fig.clear()
# fig, axs = plt.subplots(2,1, sharex=True, tight_layout=True)
axs = fig.subplots(2,1, sharex=True, gridspec_kw = {'hspace':0})
# axs = fig.get_axes()
# for ax in axs:
#     # for ln in ax.lines:
#     #     ax.lines.remove(ln)
#
#     for i in range(len(ax.lines)):
#         ax.lines.pop(i)
#
ax = axs[0]
ax1 = axs[1]
create_one_ax_plot(ax, x+5, y, ylabel='y1')
create_one_ax_plot(ax1, x+15, y, xlabel='x', ylabel='y2')

tt = time.time()

print('total time:', tt-t0)


plt.show()