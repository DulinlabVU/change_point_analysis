import numpy as np
import tqdm
import matplotlib.pyplot as plt
import os
import json

from scipy.ndimage.filters import gaussian_filter1d
from modules.hmm import HMM

def plot_trace_sections(original, smoothed, viterbi, windowlen, dir_, tr_name, fig=None, save=False):
    total = len(original)
    
    numsections = (total+windowlen-1) // windowlen
    if fig is None:
        fig4, ax4 = plt.subplots(numsections, 1, sharex=True, figsize=(10, numsections*2), dpi=400)
    else:
        fig4 = fig
        ax4 = fig4.subplots(numsections, 1, sharex=True)
    for k in range(numsections):
        start = k*windowlen
        end = np.minimum( (k+1)*windowlen, total)
        t = np.arange(start,end )
        ax4[k].plot(original[t], c='k', label='original', linewidth=0.3, alpha=0.4)
        ax4[k].plot(smoothed[t], c='k', label='smoothed', linewidth=0.8, alpha=0.6)
        ax4[k].plot(viterbi[t], c='r', label='max-likelihood', linewidth=1.2, alpha=0.7)
    ax4[0].set_title('HMM fits')
    ax4[0].legend(loc='upper right')
    if save:
        fig4.savefig(f'{dir_}hmm_fit_{tr_name}.png')
    
def viterbi_transition_matrix(ml_trace, numstates):
    """
    Generate a transition matrix by counting the max-likelihood (viterbi) states switches 
    """
    
    counts = np.zeros((numstates,numstates),dtype=np.int32)
    
    transition_events = 0
    for i in range(numstates):
        for j in range(numstates):
            counts[i,j] = np.sum((ml_trace[:-1] == i) & (ml_trace[1:] == j))
            if j != i:
                transition_events += counts[i,j]

    print(f"Max-Likelihood transition matrix based on {transition_events} transition events")
    transition_matrix = counts / np.sum(counts, 1)[:,None]
    return transition_matrix



def run_hmm(data, tr_name, fig, tr_means, dir_=None, st_prob=(0.45,0.1,0.45),
            sigma=0.05, numstates=2, freq=20, frames=40000, save=False, sequence=0):

    tr = data

    if save: ## saving the tr_means in a text file
        fname = dir_ + 'state_means.txt'
        if os.path.exists(fname):
            with open(fname, 'r') as f:
                dict_ = json.load(f)
            if tr_name not in dict_.keys():
                dict_[tr_name] = tr_means.tolist()
                with open(fname, 'w') as f:
                    json.dump(dict_, f)
        else:
            dict_ = {tr_name : tr_means.tolist()}
            with open(fname, 'w') as f:
                json.dump(dict_, f)

    smoothed = gaussian_filter1d(tr, freq)

    if sequence==0:
        fig1 = fig
        ax1 = fig1.add_subplot()
    else:
        fig1, ax1 = plt.subplots()

    ax1.hist(smoothed, bins=400)
    ax1.set_title(f'Histogram for smoothed traces {tr_name}')

    if save:
        fig1.savefig(f'{dir_}smoothed_hist_{tr_name}.png')


    # how many iterations do we update both emission prob?
    emission_prob_iterations = 1
    # how many iterations do we update transition matrix?
    baum_welch_iterations = 0

    # transition matrices per trace
    trace_tr = []

    initial_transition_prob = 1e-9

    hmm = HMM(numstates,debugMode=False)
    hmm.priors = st_prob
    hmm.tr = np.ones((numstates,numstates)) * initial_transition_prob
    hmm.tr[np.diag_indices(numstates)] = 1-initial_transition_prob

    # Get an initial estimate of posterior prob. dist. using normal distributions
    distr = hmm.getGaussianEmissionDistributions(tr_means, sigma*np.ones(numstates))
    emissionLogProb = hmm.computeEmissionLogProb(tr, distr)
    posterior = hmm.computePosterior(emissionLogProb)

    for j in range(np.max([baum_welch_iterations,emission_prob_iterations])):
        if j<baum_welch_iterations:
            emissionLogProb = hmm.computeEmissionLogProb(tr, distr)
            hmm.tr = hmm.computePosteriorTransitionProb(emissionLogProb)

        if j<emission_prob_iterations:
            distr = hmm.getEmissionDistributions(tr, posterior, bins=400,
                                             hist_smoothing=0.005, plot=False)
            updated_trace_means = np.array([distr[j].mean() for j in range(numstates)])
            print(f"Updated state mean position: {updated_trace_means}")

            if sequence == 2:
                fig2 = fig
                ax2 = fig2.add_subplot()
            else:
                fig2, ax2 = plt.subplots()

            for k in range(numstates):
                x = np.linspace(0.0, 0.4, 400)
                ax2.plot(x, distr[k].pdf(x), label=f"State {k}")
            ax2.set_title(f'Probability density per state - iteration {j}')

            if save:
                fig2.savefig(f'{dir_}statepdf_{tr_name}')

            emissionLogProb = hmm.computeEmissionLogProb(tr, distr)

    ml_trace = hmm.viterbi(emissionLogProb)

    ml_transition_matrix = viterbi_transition_matrix(ml_trace, numstates)
    trace_tr.append(ml_transition_matrix)
    print(f"Transition matrix for trace {tr_name}: {ml_transition_matrix}")

    if sequence==1:
        plot_trace_sections(tr, smoothed, tr_means[ml_trace], frames, dir_, tr_name, fig=fig, save=save)
    if save and sequence != 1:
        plot_trace_sections(tr, smoothed, tr_means[ml_trace], frames, dir_, tr_name, save=save)

    # m = trace_means[i]
    # plt.figure()
    # t = np.arange(0,10000)
    # plt.plot(t,traces[i][t],label='measured')
    # plt.plot(t,trace_means[i][ml_trace][t], label='true state')
    # #plt.plot(t,m[0]+(m[1]-m[0])*posterior[t,1], label='p(z=0)')
    # plt.plot(t,smoothed[i][t],label='smoothed')
    # plt.title(f'Max-Likelihood fit - trace {i}')
    # plt.legend()
    # plt.savefig(f'traces/mlfit{i}.png')

    if sequence==3:
        fig3 = fig
        ax3 = fig3.add_subplot()
    else:
        fig3, ax3 = plt.subplots()

    ax3.hist([smoothed[ml_trace == j] for j in range(numstates)],
             bins=30, label=[f'state {j}' for j in range(numstates)])
    ax3.legend()
    ax3.set_title('Per-state histogram of smoothed-positions')

    if save:
        fig3.savefig(f'{dir_}hist_{tr_name}.png')
        print('Figures are saved in: ..', dir_)

    # plt.show()
    plt.close('all')




if __name__ == '__main__':
    path_ = 'traces/'
    trace = np.genfromtxt(r'traces\trace_1.txt')
    trace_means = np.array([0.15,0.2,0.26])

    run_hmm(trace, 1, fig=None, tr_means=trace_means, dir_=path_,
            sigma=0.05, numstates=3, freq=20, frames=40000, save=True, sequence=-1)
