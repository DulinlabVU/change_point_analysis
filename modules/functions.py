# Created by S. C. Bera on 2021-05-13

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy import signal
import os


def str_to_num(strs, integer=False):
    """
    :param integer: return integers if True else return floats
    :param strs: str of number(s), multiple numbers can be separated by ','.
    :return: list of single or multiple number (floats or int).
    """
    num_list = []
    for item in strs.split(','):
        try:
            if integer:
                num_list.append(int(float(item)))
            else:
                num_list.append(float(item))
        except Exception as err:
            print(err)
            num_list.append(None)
            # raise err
    # print(num_list)
    return num_list


def set_xylimits(user_inputs):
    nums = str_to_num(user_inputs)
    if len(nums) == 2:
        return nums
    else:
        return [None, None]


def filter_data(data, acq_rate, freq, filter_type='lfilt'):
    """ filters the trace using a Kaiser-Bessel filter """
    # The Nyquist rate of the signal. at least half (1/2) of acq freq.
    nyq_rate = acq_rate / 2.0
    # The desired width of the transition from pass to stop
    trans_width = 0.01
    # The desired attenuation in the stop band, in dB.
    ripple_db = 100.0  # maybe reduce to 60
    # Compute the order and Kaiser parameter for the FIR filter.
    N, beta = signal.kaiserord(ripple_db, trans_width)
    # The cutoff frequency of the filter.
    cutoff = freq
    # print(f'Using filter parameters: Cutoff {freq} Hz, Ripple {ripple_db}, filter type "{filter_type}"')
    # Use firwin with a Kaiser window to create a lowpass FIR filter.
    taps = signal.firwin(N, cutoff / nyq_rate, window=('kaiser', beta))
    delay = 0.5 * (N - 1) / acq_rate

    # creating time based on acquisition frequencey
    time = np.arange(len(data)) / acq_rate  # time in sec

    filtered_z = signal.lfilter(taps, 1.0, data)
    new_pos = np.where((time - delay) >= 0)
    new_t = time[new_pos[0][0]:] - delay
    new_z = filtered_z[new_pos[0][0]:]
    if filter_type == 'lfilt':
        return new_t, new_z
    elif filter_type == 'filtfilt':
        return time, signal.filtfilt(taps, 1.0, data, padlen=len(data) - 1)
    else:
        print('Wrong filter type! Please choose "lfilt" or "filtfilt".')


def create_one_ax_plot(ax, xdata, ydata,
                      style='.-', color=None,
                      xlabel=None, ylabel=None,
                      xlim=None, ylim=None,
                      title=None, legend=None,
                      lw=None, alpha=1):
    if title:
        ax.set_title(title)
    ax.plot(xdata, ydata, style, color=color, label=legend, lw=lw, alpha=alpha)
    if xlabel:
        ax.set_xlabel(xlabel)
    if ylabel:
        ax.set_ylabel(ylabel)
    ax.set_xlim(xlim)
    ax.set_ylim(ylim)
    if legend:
        ax.set_legend()


if __name__ == '__main__':
    pass
    print(str_to_num('12.5,10', integer=False))
