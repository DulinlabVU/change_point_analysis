import numpy as np
import matplotlib.pyplot as plt
from scipy import optimize
import numpy.polynomial.polynomial as poly


def func_lin(x, m, c):
    return m*x + c

def linear_drift_corr(data):
    """
    :parameter data: 1d numpy array
    :return y: linear drift corrected 1d numpy array
    """
    x = np.arange(len(data))
    popt, pcov = optimize.curve_fit(func_lin, x, data)
    print('Fit results (m, c):', popt)
    est_drift = func_lin(x, *popt)
    ln = func_lin(x, 0, popt[1])
    err = np.subtract(est_drift, ln)
    y_cor = np.subtract(data, err)

    return y_cor, est_drift, ln,  popt


def poly_drift_corr(data, fac=20):
    """
    :param fac: order of poly fit
    :parameter data: 1d numpy array
    :return y: linear drift corrected 1d numpy array
    """
    x = np.arange(len(data))
    coeff = poly.polyfit(x, data, fac)
    print('Fit results co-eff:', coeff)
    est_drift = poly.polyval(x, coeff)
    ln = func_lin(x, 0, data[:1000].mean())
    err = np.subtract(est_drift, ln)
    y_cor = np.subtract(data, err)

    return y_cor, est_drift, err, ln,  coeff

def poly_drift_corr_by_parts(data, div=10, fac=20):
    """
    :param data: 1d numpy array
    :param div: number of segment of the trace
    :param fac: number of coeficient for poly fit
    :return: y: drift corrected 1d numpy array
    """
    cut = len(data)//div
    est_drift = np.array([])
    error = np.array([])
    y_cor = np.array([])
    for n in range(div):
        y = data[n*cut:(n+1)*cut]

        x = np.arange(len(y))
        coeff = poly.polyfit(x, y, fac)
        # print('Fit results co-eff:', coeff)
        drift = poly.polyval(x, coeff)
        ln = func_lin(x, 0, data[:500].mean())
        err = np.subtract(drift, ln)
        est_drift = np.append(est_drift, drift)
        error = np.append(error, err)
        y_cor = np.append(y_cor, np.subtract(y, err))

    return y_cor, est_drift, error

def filter_brks(brks, state_means, cutoff=0.02):
    indx = []
    ref = state_means[0]
    down = up = False
    for i in range(1, len(brks)-1):
        diff = state_means[i] - ref

        if abs(diff) < cutoff:
            indx.append(i)
            # ref = state_means[i]
        else:
            if diff > 0:
                try:
                    if state_means[i-1] - state_means[i-2] > 0:
                        if abs(high - state_means[i]) > cutoff and not down:
                            indx.append(i)
                            up = True
                            # down = False
                        else:
                            high = state_means[i]
                            ref = high
                            down = False
                    elif up:
                        indx.append(i)
                    else:
                        ref = state_means[i]
                        high = ref
                except:
                    high = state_means[i]
                    ref = high
            elif diff < 0:
                try:
                    if state_means[i-1] - state_means[i-2] < 0:
                        if abs(low - state_means[i]) > cutoff and not up:
                            indx.append(i)
                            down = True
                            # up = False
                        else:
                            ref = state_means[i]
                            low = ref
                            up = False
                    elif down:
                        indx.append(i)
                    else:
                        ref = state_means[i]
                        low = ref
                except:
                    low = state_means[i]
                    ref = low

    return np.delete(np.array(brks), indx).tolist(), np.array(brks)[indx]



if __name__ == '__main__':
    # fname = tkfd.askopenfilename()
    # fname1 = "20200324_Expt2_trace_1.txt"
    fname1 = "20200324_Expt2_trace_54bad.txt"

    y_ref = np.loadtxt(fname1)[5000:]
    # ydata = np.loadtxt(fname2)[5000:]

    # add some linear drift
    # slope = np.exp(-15)
    # intercept = 0.1
    # y_drifted = ydata + func_lin(xdata, slope, intercept)

    y_drifted = y_ref

    # y1 = poly.polyval(xdata, [5,1,0.1,1])
    # y2 = poly.polyval(xdata, [5,0,1.1,0])

    # res = poly.polyfit(xdata, y_drifted, 15)
    # print(res)
    # y2 = poly.polyval(xdata, res)


    # y_corr, drift, ln, fit_res = linear_drift_corr(y_drifted)
    y_corr, drift, error = poly_drift_corr_by_parts(y_drifted, div=5, fac=5)
    # y_corr1 = np.subtract(ydata[:len(error)], error)


    # plt.plot(xdata, y1, 'o', label='True1')
    plt.plot(y_drifted, label='Drifted data', alpha=0.3)
    # plt.plot(xdata, y2, '-', label='Poly fit')

    plt.plot(y_corr, label='Drift corr data1', alpha=0.3)
    # plt.plot(y_corr1, label='Drift corr data2', alpha=0.3)
    plt.plot(drift, label='Fit 1')
    # plt.plot(drift1, label='Fit 2')
    # # plt.plot(xdata, ydata + func_lin(xdata, -0.00001, popt[1]), '--')
    # plt.plot(ln, '--', label='Line wo drift')
    plt.legend()
    # plt.ylim(-.05, 0.2)
    plt.show()