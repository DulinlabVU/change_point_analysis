# Created by S. C. Bera on 2021-05-13

import pandas as pd


def read_mt_data(filename, name_header=True):
    """
    This reads text or csv file formats with or without headers generated in Magnetic tweezers setups in Dulin lab.
    Note: MT header format- headers: frame, time(ms), bead_no_1(x), bead_no_1(y), bead_no_1(z), ... so on.

    :param filename: typical text file generated from MT (Dulin lab)
    :param name_header: names the header if true
    :return: pandas data frame with column/header named as MT (Dulin lab) format if chosen
    """
    # reads the .csv/.txt file format
    data = pd.read_csv(filename, delimiter='\t', header=None, low_memory=True)
    if not name_header:
        return data.dropna(axis=1)
    else:
        cols = data.iloc[0, :]
        if type(cols.values[1]) is str:  # the header is selected if there is any string at the first column.
            data = pd.read_csv(filename, delimiter='\t', header=0, low_memory=True)
            # TODO: check if the column can be renamed without re-reading the file
            # try:
            #     data.columns = map(str.lower, data.columns)  # converting the column names in lowercase.
            # except:
            #     pass
            print(f'Found named data. Column names are: {data.columns.values[:6]}...')
            return data
        else:
            print("Found raw data. If there is any columns with 'NaN' it will be removed!"
                  "Named data will be returned")
            return rename_columns(data.dropna(axis=1))


def rename_columns(data):
    """ This will rename the columns as: frame, time(ms), Bead_no_1(X), Bead_no_1(Y), Bead_no_1(Z)....
    """
    if (data.shape[1] - 2) % 3 != 0:
        raise Exception('Invalid data dimension!')
    column_names = ['frame', 'time(ms)']
    for i in range(int((data.shape[1] - 2) / 3)):
        column_names.append(f'bead_{i + 1}_x')
        column_names.append(f'bead_{i + 1}_y')
        column_names.append(f'bead_{i + 1}_z')
    data.columns = column_names
    return data


def cols_names(data):
    """
    Reads data with column named as time, x, y and z or only z and separate them by name.
    :param data:
    :return: list of names according to the type (time, z, x, y)
    """
    # print('Separating column names...')
    try:
        col_names = data.columns
        time = [name for name in col_names if 'time' in name.lower()]  # lower() to avoid case mismatch
        # lists the col_names with name 'x/y/z/bead'
        if 'x' in str(col_names):
            names_x = [name for name in col_names if 'x' in name.lower()]
            names_y = [name for name in col_names if 'y' in name.lower()]
            names_z = [name for name in col_names if 'z' in name.lower()]
            # print('Found x, y and z names!')
            return time, names_z, names_x, names_y

        elif 'z' in str(col_names) and 'x' not in str(col_names):
            names_z = [name for name in col_names if 'z' in name.lower()]
            # print('Found only z names!')
            return time, names_z, None, None

        elif 'bead' in str(col_names) and 'z' not in str(col_names):
            names_z = [name for name in col_names if 'bead' in name.lower()]
            # print('Found no x/y/z but bead names!')
            return time, names_z, None, None

        else:
            print('Data cannot be extracted, suitable column name not found!')
    except AttributeError as err:
        raise err


if __name__ == '__main__':
    pass
