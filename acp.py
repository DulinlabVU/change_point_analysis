""" See about section at the end for more details...
taken from: https://matplotlib.org/2.0.2/examples/user_interfaces/embedding_in_qt5.html
modified by Subhas Ch Bera
Date: 2019-06-26
"""

# from __future__ import print_function
import os
import sys
import pickle
import pandas as pd
import numpy as np
import scipy.signal as signal
import scipy.stats as stats
from scipy import optimize
from scipy.interpolate import interp1d
import json

import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.widgets import SpanSelector
from matplotlib.figure import Figure
from matplotlib.backend_bases import key_press_handler
from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon, QIntValidator
from PyQt5.QtWidgets import QMainWindow, QFileDialog, QDialog, QApplication, QMessageBox, QStatusBar, QStyleFactory
from PyQt5.uic import loadUi

from modules.drift_corr import *
from modules import hmm_func as modules_hmm


# the main window:
def error_msg(txt):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    # str to avoid Value error as it takes str only?!
    msg.setText(str(txt))
    msg.setWindowTitle("Error!")
    msg.exec_()


def warn_msg(txt):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Warning)
    msg.setText(str(txt))
    msg.setWindowTitle("Warning!")
    msg.exec_()


def bootstrap_resample(X, n=None):
    """ Bootstrap resample an array-like
    Parameters
    ----------
    X : array_like
      data to resample
    n : int, optional
      length of resampled array, equal to len(X) if n==None
    Results
    -------
    returns X_resample
    """
    if n == None:
        n = len(X)
    X_resample = np.random.choice(X, n)
    return X_resample


def create_barplot_data(dwelltime, N):
    """
    :param dwelltime: list of dwelltimes of one condition
    :param MAX: maximum accepted dwelltime (histogram cuts off after)
    :param cutofftime: minimum accepted dwelltime (all dwelltimes below are ignored)
    :return: cut_dwelltimes, errors, cutofftime, MAX, bin_centres, counts
    """
    # cut_dwelltimes = [i for i in dwelltime if i > cutofftime] # cuts of values, which are most likely caused by glitches in the data
    # N = int(len(dwelltime)/7)
    print('Dwell times #', len(dwelltime))
    print('bins:', N)
    samples_bootstrapped = []
    tau = []
    bins = 10 ** np.linspace(np.log10(np.min(dwelltime)),
                             np.log10(np.max(dwelltime)), N)  # 50
    for i in range(0, 1000):
        dwelltime_bs = bootstrap_resample(dwelltime, n=None)
        counts, bin_edges = np.histogram(dwelltime_bs, bins=bins)
        bin_width = (bin_edges[1:] - bin_edges[:-1])
        counts = counts / bin_width / np.sum(counts)  # normalizing
        samples_bootstrapped.append(counts)
        tau.append(dwelltime_bs.mean())
    errors = np.std(samples_bootstrapped, axis=0)
    # can't add noise bigger or equal to 0.1 directly to bins as they have to increase monotonically
    counts, bin_edges = np.histogram(dwelltime, bins=bins)
    # noise  # 2. # dividing by noise so points on plot are not evenly spaced???
    bin_centres = (bin_edges[:-1] + bin_edges[1:]) / 2
    bin_width = (bin_edges[1:] - bin_edges[:-1])
    counts = counts / bin_width / np.sum(counts)
    # print('area=', np.trapz(counts, bin_centres))
    print(f'Mean dt= {np.mean(tau)} +/- {np.std(tau)}')
    return errors, bin_centres, counts, np.mean(tau), np.std(tau)


class MainWin(QMainWindow):
    """
    This creates the main window and embed matplot canvas for plotting. Also contains many functions.
    """

    # from evaluate_data import* # importing func

    def __init__(self, parent=None):
        super(MainWin, self).__init__(parent)  # modified from below
        loadUi('acp.ui', self)
        # loadUi('Qt_stable.ui', self)
        self.setWindowTitle('Analyze change points')
        self.setWindowIcon(QIcon('logo.png'))
        QApplication.setStyle(QStyleFactory.create('Fusion'))
        # self.setWindowFlags(self.windowFlags() | Qt.CustomizeWindowHint)
        # self.setWindowFlags(self.windowFlags() & ~Qt.WindowCloseButtonHint)
        # app.aboutToQuit.connect(self.close_application)
        #####################################
        ###### Creating the plot area #######
        #####################################
        # self.fig = Figure()
        # self.fig = Figure(constrained_layout=True)  # auto size, having some issue with subplots x axis overlapping
        self.fig = Figure(tight_layout=True)  # auto size
        # self.fig = Figure((3.0, 2.0), dpi=100) # fixed size
        self.canvas = FigureCanvas(self.fig)
        self.canvas.setParent(self.MainArea)
        # this is to avail the key press event of mpl
        self.canvas.setFocusPolicy(Qt.StrongFocus)
        self.canvas.setFocus()  # this is to avail the key press event of mpl
        self.mpl_toolbar = NavigationToolbar(
            self.canvas, self.MainArea)  # create the mpl toolbar
        # this actually preserves the area for plot in MainWin
        self.setCentralWidget(self.MainArea)
        self.canvas.mpl_connect('button_press_event', self.on_click)
        # mpl keypress events
        self.canvas.mpl_connect('key_press_event', self.on_key_press)
        # set plot parameters as default---
        # smaller value creates point overlapping in plot
        mpl.rcParams['agg.path.chunksize'] = 10000000
        mpl.rcParams['axes.linewidth'] = 1  # set the value globally
        mpl.rcParams['axes.labelweight'] = 'bold'  # set the value globally
        mpl.rcParams['font.sans-serif'] = 'Arial'  # set the value globally
        mpl.rcParams['font.size'] = 12  # set the value globally
        mpl.rcParams['font.weight'] = 'bold'  # set the value globally
        mpl.rcParams['xtick.direction'] = 'in'  # set the value globally
        mpl.rcParams['xtick.labelsize'] = 12  # set the value globally
        # mpl.rcParams['xtick.major.width'] = 1  # set the value globally
        mpl.rcParams['ytick.direction'] = 'in'  # set the value globally
        mpl.rcParams['ytick.labelsize'] = 12  # set the value globally
        # mpl.rcParams['ytick.major.width'] = 1  # set the value globally
        mpl.rcParams['pdf.fonttype'] = 42
        mpl.rcParams['xtick.minor.visible'] = True  # set the value globally
        mpl.rcParams['ytick.minor.visible'] = True  # set the value globally

        # print(mpl.rcParams.keys())
        ###### Adding mpl toolbar and canvas to Layout ######
        self.Frame1VLayout.addWidget(self.mpl_toolbar)
        self.Frame1VLayout.addWidget(self.canvas)

        ###################################
        ######### Menu items  #############
        ###################################
        ## to open file ##
        # perform the action by calling the function.
        self.actionOpen.triggered.connect(self.open_file)
        ## to save file ##
        self.actionSave.triggered.connect(self.save_file)
        self.actionChoose_dir.triggered.connect(self.choose_save_dir)
        ## Others
        self.actionClear.triggered.connect(self.init_param)
        ## to quite ##
        # self.actionExit.triggered.connect(self.close_application)
        self.actionAbout.triggered.connect(self.about)

        # https://stackoverflow.com/questions/19646185/python-qt-updating-status-bar-during-program-execution
        # self.statusbar = QStatusBar()
        self.statusbar1 = QStatusBar()
        self.statusbar2 = QStatusBar()
        # this align to the right of stat.bar
        self.statusbar.addPermanentWidget(self.statusbar1)
        # this align to the right of stat.bar1
        self.statusbar.addPermanentWidget(self.statusbar2)
        # self.setStatusBar(self.statusbar)
        self.statusbar.showMessage('Ready for status tips!')
        self.statusbar1.showMessage('Ready!')
        self.statusbar2.showMessage('Ready!')

        ########### Button actions #############
        self.OpenBtn.clicked.connect(self.open_file)
        self.SelectBtn.clicked.connect(self.select_file)
        self.SaveDataBtn.clicked.connect(self.save_file)
        self.SaveDtBtn.clicked.connect(self.save_dt)
        # self.SaveStBtn.clicked.connect(self.save_st)
        self.SaveFigBtn.clicked.connect(self.save_fig)
        self.PlotPrevBtn.clicked.connect(self.plot_prev)
        self.PlotNextBtn.clicked.connect(self.plot_next)
        self.PlotRefreshBtn.clicked.connect(self.call_plots)
        # self.PlotHistBtn.clicked.connect(self.plot_hist)
        # self.PlotHistStBtn.clicked.connect(self.plot_hist_st)
        self.PlotDtBtn.clicked.connect(self.plot_dt)
        self.DelDataBtn.clicked.connect(self.del_CPA)
        self.DelStateBtn.clicked.connect(self.del_state)
        self.DelBrkBtn.clicked.connect(self.del_brk)
        self.UndoDelBrkBtn.clicked.connect(self.undo_del_brk)

        ### Line edits ###
        self.setPlotNo.setValidator(QIntValidator())
        self.setPlotNo.editingFinished.connect(self.set_plot_no)
        self.setYlim.editingFinished.connect(self.set_ylim)
        self.setXlim.editingFinished.connect(self.set_xlim)
        self.setOffset.editingFinished.connect(self.set_offset)
        self.setSkipBrk.editingFinished.connect(self.skip_brk)
        # self.setNonLinDrift.editingFinished.connect(self.set_drift)
        self.setStMeans.editingFinished.connect(self.set_StMeans)
        self.setSigma.editingFinished.connect(self.set_sigma)
        self.setNumSt.editingFinished.connect(self.set_numstates)
        self.setFrames.editingFinished.connect(self.set_frames)
        self.setStateProb.editingFinished.connect(self.set_state_prob)

        ### Check boxes ###
        self.AnalizeCPADataGBox.clicked.connect(self.call_plots)
        self.SepStatecheckBox.clicked.connect(self.call_plots)
        self.HMMGB.toggled.connect(self.call_plots)

        self.HMMcomB.addItem('Smoothed hist')
        self.HMMcomB.addItem('HMM fit')
        self.HMMcomB.addItem('Per-state hist')
        self.HMMcomB.addItem('Prob density')
        # self.HMMcomB.addItem('HMM fit')
        self.HMMcomB.activated[str].connect(self.hmm_meth)
        self.hmm_meth_no = 0

        self.init_param()

    def init_param(self):
        # self.fig.clear()
        self.data = {}
        self.data_status = None
        self.plot_no = 0
        self.btm = None
        self.top = None
        self.dict_save = {}
        self.save_data = {}
        self.cutoff_hz = 1  # 0.5
        self.dec_fac = 10
        self.offset = 0
        self.aq_freq = 58
        self.bin_o = 10
        self.bin_c = 10
        self.skip_bkp = []
        self.x_l = None
        self.x_r = None
        # self.st_means = np.array([])

    def hmm_meth(self):
        self.hmm_meth_no = self.HMMcomB.currentIndex()
        self.call_plots()

    def choose_save_dir(self):
        fout = QFileDialog.getExistingDirectory(self, 'Select directory to save')
        if fout == '':
            self.fout = 'traces/'
        else:
            self.fout = fout + '/'
        print(self.fout)

    def set_StMeans(self):
        """define the approximate mean values of the states used in hmm"""
        try:
            self.tr_means = np.array([float(i) for i in self.setStMeans.text().split(',')])
        except:
            error_msg('Wrong input!')

    def set_state_prob(self):
        """define the approximate probability values of the states used in hmm"""
        try:
            self.st_prob = [float(i) for i in self.setStateProb.text().split(',')]
        except:
            error_msg('Wrong input!')

    def set_sigma(self):
        """define the sigma values used in hmm"""
        try:
            self.sigma = float(self.setSigma.text())
        except:
            error_msg('Wrong input!')

    def set_numstates(self):
        """define the approximate number of the states used in hmm"""
        try:
            self.numstates = int(self.setNumSt.text())
        except:
            error_msg('Wrong input!')

    def set_frames(self):
        """define the range of frame used in hmm"""
        try:
            self.frames = int(self.setFrames.text())
        except:
            error_msg('Wrong input!')

    def skip_brk(self):
        """reads the break points need to be skipped while calculating dwell times"""
        try:
            self.skip_bkp = self.setSkipBrk.text().split(',')
            self.call_plots()
        except:
            error_msg('Wrong input!')

    def set_offset(self):
        try:
            self.offset = float(self.setOffset.text())
            self.call_plots()
        except:
            self.setOffset.setText(str(self.offset))
            error_msg('Wrong input!')

    def set_plot_no(self):
        try:
            self.plot_no = int(self.setPlotNo.text()) - 1
            if self.plot_no + 1 > self.plot_total or self.plot_no < 0:
                error_msg(
                    f'Plot no. should be within 1 and {self.plot_total}')
            else:
                self.call_plots()
        except ValueError:
            pass

    def set_drift(self):
        try:
            self.div, self.ordr = [int(i) for i in self.setNonLinDrift.text().split(',')]
        except Exception as err:
            error_msg(err)

    def set_ylim(self):
        lims = self.setYlim.text().split(',')
        try:
            self.btm = float(lims[0])
            self.top = float(lims[1])
        except:
            self.btm = None
            self.top = None
            # self.error_msg(f'Wrong input: {err}')
        self.call_plots()

    def set_xlim(self):
        if self.setXlim.text() == '':
            self.x_l = None
            self.x_r = None
        else:
            lims = self.setXlim.text().split(',')
            try:
                self.x_l = float(lims[0])
            except Exception as err:
                self.x_l = None
                error_msg(f'Wrong input: {err}')
            try:
                self.x_r = float(lims[1])
            except Exception as err:
                self.x_r = None
                error_msg(f'Wrong input: {err}')
        self.call_plots()

    def call_plots(self):
        try:
            if self.HMMGB.isChecked() and self.check_data() == 'OK':
                self.plot_hmm()
                self.update_progressBar()
            elif self.AnalizeCPADataGBox.isChecked() and self.check_data() == 'OK':
                self.plot_cpa()
                self.update_progressBar()
            else:
                self.plot_data()
                self.update_progressBar()
        except Exception as err:
            error_msg(f'Plot error: {err}')

    def open_file(self):
        name, _ = QFileDialog.getOpenFileName(self, 'Open File', '*.pickle')

        self.filein = name
        print(name)
        if name:
            if len(self.data) > 0:
                if self.msg_box('Want to clear old data?') == QMessageBox.Yes:
                    self.init_param()
            self.read_data()

    def read_data(self):
        """  This reads text or pickle file formats.
        """
        filename = self.filein
        self.statusbar.showMessage('Reading data...please wait!')
        if 'pickle' in filename:
            try:  # reads the .pickle file format
                self.data = pd.read_pickle(filename)
                self.beads = list(self.data.keys())
                self.ListTrace.setText(str(self.beads))
                self.plot_total = len(self.beads)
                self.statusbar2.showMessage(
                    f'Total no of dataset: {self.plot_total}')
                self.call_plots()
            except Exception as err:
                error_msg(f'This is not a suitable data file/format: {err}')
        else:
            error_msg(
                "Please check the filename/path/format. It must have .txt/.pickle extension!")

    def check_data(self):
        try:
            if self.plot_total > 0:
                self.data_status = 'OK'
                return 'OK'
        except:
            error_msg('No data found! Please choose data first.')

    def plot_next(self):  # inherited form Mona
        if self.data_status or self.check_data() == 'OK':
            if self.ZoomcheckBox.isChecked():
                left, right = self.fig.gca().get_xlim()
                x_dif = right - left
                # self.fig.gca().set_xlim((left + x_dif - 5), (right + x_dif))
                self.fig.get_axes()[0].set_xlim((left + x_dif - 5), (right + x_dif))
                self.fig.get_axes()[1].set_xlim((left + x_dif - 5), (right + x_dif))
                self.canvas.draw()
            elif self.plot_no + 1 >= self.plot_total:
                if self.msg_box('This was the last dataset! Want to plot from the START?') == QMessageBox.Yes:
                    self.plot_no = 0
                    self.call_plots()
            else:
                self.plot_no += 1
                # plot is called upon with updated ii_plot, such that you will plot the next column in the dataset
                self.call_plots()

    def plot_prev(self):
        if self.data_status or self.check_data() == 'OK':
            if self.ZoomcheckBox.isChecked():
                left, right = self.fig.gca().get_xlim()
                x_dif = right - left
                # self.fig.gca().set_xlim((left - x_dif), (right - x_dif + 5))
                self.fig.get_axes()[0].set_xlim((left - x_dif), (right - x_dif + 5))
                self.fig.get_axes()[1].set_xlim((left - x_dif), (right - x_dif + 5))
                self.canvas.draw()
            elif self.plot_no - 1 < 0:
                if self.msg_box('This was the first dataset! Want to plot from the END?') == QMessageBox.Yes:
                    self.plot_no = self.plot_total - 1
                    self.call_plots()
            else:
                self.plot_no -= 1
                # plot is called upon with updated plot_no, such that you will plot the next column in the dataset
                self.call_plots()

    def plot_hmm(self):
        self.fig.clear()
        self.set_StMeans()
        self.set_sigma()
        self.set_numstates()
        self.set_frames()
        self.set_state_prob()

        tr_name = self.beads[self.plot_no]
        tr_data = self.data[tr_name]['raw_data_cut'][2]

        if self.SaveHMMCB.isChecked():
            save = True
            try:
                dir_ = self.fout
            except:
                self.choose_save_dir()
                dir_ = self.fout

            modules_hmm.run_hmm(tr_data, tr_name, self.fig, self.tr_means, sigma=self.sigma, numstates=self.numstates,
                                freq=self.cutoff_hz, frames=self.frames, dir_=dir_, sequence=self.hmm_meth_no,
                                st_prob=self.st_prob, save=save)
        else:
            modules_hmm.run_hmm(tr_data, tr_name, self.fig, self.tr_means, sigma=self.sigma, numstates=self.numstates,
                                freq=self.cutoff_hz, frames=self.frames, sequence=self.hmm_meth_no,
                                st_prob=self.st_prob)
        self.canvas.draw()

    def plot_data(self):
        # self.xlim = self.fig.gca().get_xlim()
        calib_data = self.data[self.beads[self.plot_no]]['raw_data_cut'][0]
        calib_time = self.data[self.beads[self.plot_no]]['raw_data_cut'][1]
        calib_time = calib_time - calib_time[0]  # time starts from 0
        dec_calib, dec_calib_time = self.dec_data(calib_time, calib_data)

        trace_data = self.data[self.beads[self.plot_no]]['raw_data_cut'][2]
        data_time = self.data[self.beads[self.plot_no]]['raw_data_cut'][3]
        data_time = data_time - data_time[0]  # time starts from 0
        dec_trace = self.data[self.beads[self.plot_no]]['decimated_trace'][0]
        dec_trace_time = self.data[self.beads[self.plot_no]]['decimated_trace'][1]
        dec_trace_time = dec_trace_time - dec_trace_time[0]

        self.my_bkps = self.data[self.beads[self.plot_no]]['break_points']
        try:
            self.readPenVal.setText(str(self.data[self.beads[self.plot_no]]['CPA_info'][2]))
        except:
            self.readPenVal.setText('Not available!')

        self.fig.clf()
        ax1 = self.fig.add_subplot(2, 1, 1)
        ax2 = self.fig.add_subplot(2, 1, 2)
        # if self.ZoomcheckBox.isChecked():
        #     ax2.set_xlim(self.xlim)
        ax1.plot(calib_time / 1000, calib_data, '-', color='gray', label='Calibration raw data', lw=0.5)
        ax1.plot(dec_calib_time / 1000, dec_calib, '-', color='black', label='Dec data', lw=0.5)
        ax2.plot(data_time[:len(trace_data)] / 1000, trace_data, '-', color='gray', label='Trace raw data', lw=0.5)
        ax2.plot(dec_trace_time / 1000, dec_trace, '-', color='black', label='Dec data', lw=0.5)
        if len(self.my_bkps) > 0 and self.ShowBkpscheckBox.isChecked():
            for i in range(len(self.my_bkps)):
                bkp = self.my_bkps[i] * self.dec_fac / 58 + dec_trace_time[0] / 1000
                ax2.axvline(x=bkp, color='r', lw=0.5)
                if self.LabelBkpscheckBox.isChecked():
                    ax2.text(bkp, np.mean(dec_trace), f'{i + 1}', color='c')
        if self.ShowTitlecheckBox.isChecked():
            ax1.set_title(f'Plot of dataset no. {self.plot_no + 1} ({self.beads[self.plot_no]}) of {self.plot_total}')
        ax1.set_ylabel('Z pos ($\mu$m)')
        ax2.set_ylabel('Z pos ($\mu$m)')
        ax2.set_xlabel('Time (s)')
        # ax1.set_ylim(bottom=self.btm, top=self.top)
        ax2.set_ylim(bottom=self.btm, top=self.top)
        ax2.set_xlim(self.x_l, self.x_r)
        # ax1.legend(loc='lower center')
        # ax2.legend(loc='upper right')
        self.canvas.draw()

    def plot_cpa(self):
        self.xlim = self.fig.gca().get_xlim()
        self.fig.clf()
        axes = self.fig.subplots(2, 1, sharex=True)
        ax1 = axes[0]
        self.ax1 = ax1
        ax2 = axes[1]

        self.trace_data = self.data[self.beads[self.plot_no]]['raw_data_cut'][2]
        self.dec_trace = self.data[self.beads[self.plot_no]]['decimated_trace'][0]

        # if not self.ApndStcheckBox.isChecked():
        #     self.st_means = np.array([])

        self.my_bkps = self.data[self.beads[self.plot_no]]['break_points']
        # self.my_bkps_old = self.my_bkps # to avoid mess with previous trace
        self.state_cutoff = np.mean(self.dec_trace) - self.offset
        states_dict = self.make_state_dict()
        ax1.plot(self.dec_trace, '-', markersize=1, label='Dec data', alpha=0.5, lw=0.5)
        ax1.plot([0, len(self.dec_trace)], [self.state_cutoff] * 2, 'k--')
        for k in states_dict:
            ax1.plot(k, [np.median(states_dict[k])] * 2, 'r-')  # uses the median
            # self.st_means = np.append(self.st_means, (np.median(states_dict[k])))
        # ax1.plot(k, [states_dict[k].mean()]*2, 'r-') # uses the mean
        # self.st_means = np.append(self.st_means, (states_dict[k].mean()))
        ax2.plot(self.dec_trace, '-', markersize=1, label='Dec data', lw=0.5)
        if len(self.my_bkps) > 0 and self.ShowBkpscheckBox.isChecked():
            for i in range(len(self.my_bkps)):
                ax2.axvline(x=self.my_bkps[i], color='k', zorder=5, lw=0.5)
                if self.LabelBkpscheckBox.isChecked():
                    ax2.text(self.my_bkps[i], np.mean(self.dec_trace), f'{i + 1}')
        ax2.set_xlabel('frame')
        ax2.set_ylabel('z-position ($\mu$m)')
        ax1.set_ylabel('z-position ($\mu$m)')
        ax1.set_ylim(bottom=self.btm, top=self.top)
        ax2.set_ylim(bottom=self.btm, top=self.top)
        ax1.set_xlim(self.x_l, self.x_r)
        ax2.set_xlim(self.x_l, self.x_r)
        if self.ZoomcheckBox.isChecked():
            ax1.set_xlim(self.xlim)
            ax2.set_xlim(self.xlim)
        ax1.legend(loc='upper right')
        ax2.legend(loc='upper right')
        self.canvas.draw()

    def plot_hist(self):
        self.append_states()
        self.fig.clf()
        ax1 = self.fig.add_subplot(122)
        ax2 = self.fig.add_subplot(121)

        errors, bin_centres, counts, self.tau0, self.tau0_sd = create_barplot_data(self.close_states, N=10)
        ax1.errorbar(bin_centres, counts, fmt='o', color='gray', mfc='none', lw=1, yerr=2 * errors, capsize=2,
                    capthick=1, zorder=1, label='CS dts')  # 2*errors = 2sd = 95% CI

        errors, bin_centres, counts, self.tau01, self.tau01_sd = create_barplot_data(self.open_states, N=10)
        ax2.errorbar(bin_centres, counts, fmt='o', color='gray', mfc='none', lw=1, yerr=2 * errors, capsize=2,
                    capthick=1, zorder=1, label='OS dts')  # 2*errors = 2sd = 95% CI

        ax1.set_xlabel('Dwell time (s)')
        ax2.set_xlabel('Dwell time (s)')
        ax2.set_ylabel('# Events')
        ax1.set_xscale('log')
        ax2.set_xscale('log')
        ax1.set_yscale('log')
        ax2.set_yscale('log')
        ax1.legend()
        ax2.legend()
        self.canvas.draw()
        self.save_dt()

    def plot_dt(self):
        self.append_states()
        if len(self.open_states) == 0 or len(self.close_states) == 0:
            warn_msg('No dwell time found for open/close state!')
        else:
            self.plot_hist()

    def make_state_dict(self):
        state_dict = {}
        if self.AddZerocheckBox.isChecked():
            k = 0, self.my_bkps[0]
            state = self.dec_trace[0:self.my_bkps[0]]
            # if self.PlotRawcheckBox.isChecked():
            # k = 0 * self.dec_fac, self.my_bkps[0] * self.dec_fac
            # state = self.dec_trace[0:self.my_bkps[0]]
            state_dict[k] = state
        for i in range(len(self.my_bkps) - 1):
            # if str(i + 1) in self.skip_bkp: # to skip the rest of bkp
            if str(i + 1) not in self.skip_bkp:  # to skip the desired bkp
                k = self.my_bkps[i], self.my_bkps[i + 1]
                state = self.dec_trace[self.my_bkps[i]:self.my_bkps[i + 1]]
                # if self.PlotRawcheckBox.isChecked():
                # k = self.my_bkps[i]*self.dec_fac, self.my_bkps[i+1]*self.dec_fac
                # state = self.dec_trace[self.my_bkps[i]*self.dec_fac: self.my_bkps[i+1]*self.dec_fac]
                state_dict[k] = state
        self.sep_state(state_dict)
        return state_dict

    def sep_state(self, states):
        if self.SepStatecheckBox.isChecked() and self.AnalizeCPADataGBox.isChecked():
            os = []
            cs = []
            for k in states:
                if self.state_cutoff > states[k].mean():
                    # converting to time (s) from frame #
                    dt_o = np.diff(k) * self.dec_fac * (1 / self.aq_freq)
                    os.extend(np.around(dt_o, 2))
                elif self.state_cutoff < states[k].mean():
                    # converting to time (s) from frame #
                    dt_c = np.diff(k) * self.dec_fac * (1 / self.aq_freq)
                    cs.extend(np.around(dt_c, 2))
            self.dict_save[self.beads[self.plot_no]] = {'open state': os, 'close state': cs,
                                                        'cutoff': round(self.state_cutoff, 2), 'offset': self.offset}
            self.ListAnalTrace.setText(str(list(self.dict_save.keys())))

    def append_states(self):
        self.open_states = []
        self.close_states = []
        for bead in self.dict_save:
            try:
                self.open_states.extend(self.dict_save[bead]['open state'])
                self.close_states.extend(self.dict_save[bead]['close state'])
            except Exception as err:
                error_msg(f'Error in appending states! Error: {err}')
        self.open_states = np.array(self.open_states)

    def del_CPA(self):
        if self.msg_box('Delete current dataset?') == QMessageBox.Yes:
            k = self.beads[self.plot_no]
            if k in self.data:
                del self.data[k]
                self.beads = list(self.data.keys())
                self.plot_total = len(self.beads)
                self.ListTrace.setText(str(self.beads))
                self.statusbar2.showMessage(
                    f'Total no of dataset: {self.plot_total}')
                self.statusbar.showMessage(
                    'Current analysis successfully deleted!')
                self.call_plots()
            else:
                error_msg('No data found with current file name!')
        else:
            pass

    def del_state(self):
        if self.msg_box('Delete current states?') == QMessageBox.Yes:
            name = self.beads[self.plot_no]
            if name in self.dict_save:
                del self.dict_save[name]
                self.statusbar.showMessage(
                    'Current states successfully deleted!')
                self.ListAnalTrace.setText(str(list(self.dict_save.keys())))
            else:
                error_msg('No data found with current trace name!')
        else:
            pass

    def del_brk(self):
        self.my_bkps_old = dict({self.beads[self.plot_no]: self.my_bkps[:]})  # making a copy by ':'
        list_del_brl = self.ReadBrkPt.text()
        if len(list_del_brl)>1:
            if '-' in self.ReadBrkPt.text():
                a, b = self.ReadBrkPt.text().split('-')
                if a == '':
                    try:
                        lst = list(range(int(b) + 1))
                    except:
                        lst = []
                        error_msg('Unable to make the list!')
                elif b == '':
                    try:
                        lst = list(range(int(a), len(self.my_bkps) + 1))
                    except:
                        lst =[]
                        error_msg('Unable to make the list!')
                else:
                    try:
                        lst = list(range(int(a), int(b) + 1))
                    except:
                        lst = []
                        warn_msg('Provide a range with two numbers separated by -')

            elif ',' in self.ReadBrkPt.text():
                lst = self.ReadBrkPt.text().split(',')
        elif len(list_del_brl) == 1:
            try:
                lst = [int(list_del_brl)]
            except:
                pass
        else:
            warn_msg('No suitable format provided. Either use "-" to define a range or "," to separate numbers')

        try:
            while lst:
                indx = lst.pop()
                try:
                    no = int(indx) - 1
                    del self.my_bkps[no]
                except Exception as err:
                    error_msg(err)
            self.data[self.beads[self.plot_no]]['break_points'] = self.my_bkps
            self.call_plots()
        except Exception as e:
            error_msg(e)

    def undo_del_brk(self):
        try:
            if self.beads[self.plot_no] in self.my_bkps_old:
                self.my_bkps = self.my_bkps_old[self.beads[self.plot_no]]
                self.data[self.beads[self.plot_no]]['break_points'] = self.my_bkps
                self.call_plots()
            else:
                warn_msg('No break points deleted!')
        except Exception as err:
            warn_msg(err)

    def add_txt(self, txt):
        self.ListAnalTrace.append('-----------------')
        self.ListAnalTrace.append(txt)

    def dec_data(self, x, y):
        """ decimate the data, input: numpy array, returns: numpy array """
        # decimate calibration data
        try:
            self.dec_fac = self.data[self.beads[self.plot_no]]['CPA_info'][3]
        except:
            self.dec_fac = 10
        data = y
        data_time = x
        dec_data = np.array([])
        dec_data_time = np.array([])
        for i in range(int(len(data) / self.dec_fac)):
            try:
                dec_data_time = np.append(
                    dec_data_time, data_time[(i + 1) * self.dec_fac])
                dec_data = np.append(
                    dec_data, data[i * self.dec_fac: (i + 1) * self.dec_fac].mean())
            except:
                pass  # this will discard the end points that less than decimation factor
        return dec_data, dec_data_time

    def select_file(self):
        if self.beads[self.plot_no] not in self.save_data:
            self.save_data[self.beads[self.plot_no]] = self.data[self.beads[self.plot_no]]
            self.statusbar.showMessage(f'{self.beads[self.plot_no]} successfully selected!')
        else:
            warn_msg('The file is already selected!')

    def save_file(self):
        try:
            if os.path.split(self.filein)[0] in self.fileout:
                path = self.fileout
            else:
                path = self.filein
        except AttributeError:
            path = self.filein
        except:
            error_msg('Unable to save file')
        f_out, _ = QFileDialog.getSaveFileName(self, 'Save file', path, '*.txt *.csv *.pickle')
        # QFileDialog.getSaveFileName(self, 'Save File', options=QFileDialog.DontUseNativeDialog)

        self.fileout = f_out
        self.statusbar.showMessage(f'Output file name: {f_out}')
        if f_out == '':
            pass
        elif '.txt' in self.fileout:
            save_data = pd.DataFrame()
            for k in self.data:
                trace = pd.DataFrame(self.data[k]['raw_data_cut'][2])
                try:
                    # ignore_index to allow different shaped data
                    save_data = pd.concat([save_data, trace], ignore_index=True, axis=1)
                    # print(data.shape)
                except Exception as err:
                    error_msg(err)
            save_data.to_csv(path_or_buf=self.fileout, sep='\t', header=False, index=False)
            warn_msg('Raw data successfully saved as a txt file!')
        elif '.pkl' in self.fileout or 'pickle' in self.fileout:
            with open(self.fileout, 'wb') as myfile:
                if len(self.save_data) == 0:
                    pickle.dump(self.data, myfile)
                    warn_msg('Data successfully saved as a pickle file!')
                else:
                    if self.msg_box('Want to save selected trace(s)?') == QMessageBox.Yes:
                        pickle.dump(self.save_data, myfile)
                        warn_msg('Selected data successfully saved as a pickle file!')
                    else:
                        pickle.dump(self.data, myfile)
                        warn_msg('Data successfully saved as a pickle file!')
        else:
            error_msg('Unable to save! check file format/path!')

    def save_fig(self):
        try:
            if os.path.split(self.filein)[0] in self.fileout:
                path = self.fileout
            else:
                path = self.filein
        except:
            path = self.filein
        f_out, _ = QFileDialog.getSaveFileName(
            self, 'Save current figure', path, "Images (*.png *.tiff *.jpg *.pdf)")
        self.fileout = f_out
        if f_out == '':
            pass
        else:
            try:
                self.fig.savefig(f_out)
            except Exception as err:
                error_msg(f'Unable to save figure: {err}')

    def save_dt(self):
        try:
            self.dict_save['Info'] = {'names': self.beads, 'total OS': len(self.open_states),
                                      'total CS': len(self.close_states), 'Bins(O/C)': [self.bin_o, self.bin_c],
                                      'dt_o': round(self.tau0, 2), 'dt_o_sd': round(self.tau0_sd, 2),
                                      'dt_c': round(self.tau01, 2), 'dt_c_sd': round(self.tau01_sd, 2),
                                      # 'Dwell time cutoff [CS,OS(max,min)]': [(self.cs_max, self.cs_min),
                                      #                                        (self.os_max, self.os_min)],
                                      # 'Fit guess': self.guess.tolist(),
                                      }
        except Exception as err:
            warn_msg(f'Click Plot dwell times first: {err}')
        with open(f'{self.filein[:-7]}_dts.txt', 'w') as sf:
            json.dump(self.dict_save, sf)

        try:
            np.savetxt(f'{self.filein[:-7]}_open_state_dts_(#{len(self.open_states)}).txt', self.open_states,
                       fmt='%0.5f')
            np.savetxt(f'{self.filein[:-7]}_close_state_dts_(#{len(self.close_states)}).txt', self.close_states,
                       fmt='%0.5f')
            warn_msg("Dwell times (CS & OS) are successfully saved in the source data folder!")
        except Exception as err:
            error_msg(err)

    def update_progressBar(self):
        try:
            # as plot_no starts at 0.
            done = 100 * (self.plot_no + 1) // (self.plot_total)
            # set the value of the progressBarbar to a % of the total dataset
            self.progressBar.setValue(done)
            self.statusbar1.showMessage(
                f'Plot # {self.plot_no + 1} ({self.beads[self.plot_no]}) on display')
        except Exception as err:
            error_msg(f'Error updating progressBar: {err}')

    def on_key_press(self, event):
        k = event.key
        if k == 'right':
            # print(f'Pressed {k} key.')
            self.plot_next()
        if k == 'left':
            # print(f'Pressed {k} key.')
            self.plot_prev()
        # implement the default mpl key press events described at
        # http://matplotlib.org/users/navigation_toolbar.html#navigation-keyboard-shortcuts
        key_press_handler(event, self.canvas, self.mpl_toolbar)

    def on_click(self, event):
        # print(event.xy)
        pass

    def msg_box(self, msg):
        choice = QMessageBox.question(self, 'Message', msg,
                                      QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        return choice

    def closeEvent(self, event):
        choice = QMessageBox.question(self, 'Message',
                                      "Are you sure to close?", QMessageBox.Yes |
                                      QMessageBox.No, QMessageBox.No)
        if choice == QMessageBox.Yes:
            print('Window closed!')
            event.accept()
        else:
            event.ignore()

    def about(self):
        QMessageBox.about(self, "About",
                          """embedding matplot in qt5 an example
        Copyright 2015 BoxControL

        This program is inherited from a simple example of a Qt5 application
        embedding matplotlib canvases. It is base on example from matplolib
        documentation, and initially was developed from Florent Rougon and Darren
        Dale. Modified by Subhas Ch Bera, IZKF, FAU, Germany.

        https://matplotlib.org/2.0.2/examples/user_interfaces/embedding_in_qt5.html

        It may be used and modified with no restriction; raw copies as well as
        modified versions may be distributed without limitation."""
                          )


def run():
    app = QApplication(sys.argv)
    Gui = MainWin()
    Gui.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    run()
