import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy import optimize
import scipy.stats as stats
import tkinter.filedialog as tkfd
import tkinter.messagebox as tkmsg
import os

## formatting the figure look and fonts for saving ##
mpl.rcParams['figure.figsize'] = [2.66, 1.75]  # in cm it is 9/6 (w/h)
# mpl.rcParams['figure.figsize'] = [3.54, 2.36]  # for fig3
# mpl.rcParams['figure.figsize'] = [1.7,1.6]  # in cm it is 9/6 (w/h)
# mpl.rcParams['axes.linewidth'] = 1  # set the value globally
mpl.rcParams['axes.labelweight'] = 'bold'  # set the value globally
mpl.rcParams['font.sans-serif'] = 'Arial'  # set the value globally
mpl.rcParams['font.size'] = 12  # set the value globally
mpl.rcParams['font.weight'] = 'bold'  # set the value globally
mpl.rcParams['xtick.top'] = True  # set the value globally
mpl.rcParams['xtick.direction'] = 'in'  # set the value globally
mpl.rcParams['xtick.labelsize'] = 11  # set the value globally
# mpl.rcParams['xtick.labeltop'] = True  # set the value globally
# mpl.rcParams['xtick.labelbottom'] = False  # set the value globally
# mpl.rcParams['xtick.major.width'] = 1  # set the value globally
# mpl.rcParams['xtick.major.size'] = 5  # set the value globally
mpl.rcParams['ytick.right'] = True  # set the value globally
# mpl.rcParams['ytick.labelleft'] = False  # set the value globally
mpl.rcParams['ytick.direction'] = 'in'  # set the value globally
mpl.rcParams['ytick.labelsize'] = 11  # set the value globally
# mpl.rcParams['ytick.major.width'] = 1  # set the value globally
# mpl.rcParams['ytick.major.size'] = 5  # set the value globally
mpl.rcParams['svg.fonttype'] = 'none'  # this works for font edit in AI
mpl.rcParams['pdf.fonttype'] = 42  # this works for font edit in AI
mpl.rcParams['xtick.minor.visible'] = True  # set the value globally
mpl.rcParams['ytick.minor.visible'] = True  # set the value globally


def bootstrap(X, n=None):
    """ Bootstrap resample an array-like
    Parameters
    ----------
    X : array_like
      data to resample
    n : int, optional
      length of re-sampled array, equal to len(X) if n==None
    -------
    returns X_resample
    """
    if n is None:
        n = len(X)
    X_resample = np.random.choice(X, n)
    return X_resample


def create_barplot_data(dwelltime, N=10, cutoff_low=0.5, cutoff_high=5000):
    """
    :param dwelltime: list of dwelltimes of one condition
    :param N: number of bins to plot the histogram
    :param MAX: maximum accepted dwelltime (histogram cuts off after)
    :param cutoff_low: minimum accepted dwelltime (all dwelltimes below are ignored)
    :param cutoff_high: maximum accepted dwelltime (all dwelltimes above are ignored)
    :return: cut_dwelltimes, errors, cutofftime, MAX, bin_centres, counts
    """
    ## filtering the dwell times with low and high cutoff
    cut_dt = dwelltime[(cutoff_low < dwelltime) & (dwelltime < cutoff_high)]

    results = fit_exp2(cut_dt).x
    # n = 1000
    # N = int(len(dwelltime)/7)
    print('Dwell times #', len(cut_dt))
    print('Dwell times #', sorted(cut_dt)[:10])
    print('...', sorted(cut_dt)[-10:])
    print('bins:', N)
    print('Cutoffs (low, high):', cutoff_low, cutoff_high)

    print('Results:', results)

    samples_bootstrapped = []
    results_bs = []
    bns = 10 ** np.linspace(np.log10(np.min(cut_dt)), np.log10(np.max(cut_dt)), N)  # 50
    counts, bin_edges = np.histogram(cut_dt, bins=bns)
    bin_centre = (bin_edges[:-1] + bin_edges[1:]) / 2
    bin_width = (bin_edges[1:] - bin_edges[:-1])
    count = counts / bin_width / np.sum(counts)
    for i in range(0, 1000):
        dwell_bs = bootstrap(cut_dt, n=None)
        counts_bs, bin_edges = np.histogram(dwell_bs, bins=bns)
        bin_width = (bin_edges[1:] - bin_edges[:-1])
        counts_bs = counts_bs / bin_width / np.sum(counts_bs)  ## normalizing

        samples_bootstrapped.append(counts_bs)
        results_bs.append(fit_exp2(dwell_bs).x)
        # res_bs = np.vstack(
        #     (res_bs, fit_exp2(dwell_bs).x))  ## append or concatenate will not work straight for 1D array
        ## https://stackoverflow.com/questions/21887754/concatenate-two-numpy-arrays-vertically

    err_bs = np.std(samples_bootstrapped, axis=0)  # mean + sigma = 64 % CI, mean + 2*sigma = 95% CI
    return cut_dt, err_bs, bin_centre, count, results, results_bs


def fit_exp2(y):
    def MLE(params):
        """ find the max likelihood """
        a, k1, k2 = params
        yPred = (1 - a) * k1 * np.exp(-k1 * y) + a * k2 * np.exp(-k2 * y)
        LL = np.sum(np.log(yPred))
        BIC = len(params) * np.log(len(y)) - 2 * LL
        # print('BIC:',BIC)
        return -LL

    guess = np.array([1, 1, 1])
    bnds = ((0.05, 0.9), (0.001, 0.2), (0.0001, 0.02))  # with lower bounds for the guesses
    # print(bnds)
    # bnds = ((0.05, 0.99), (1 / 1000, 0.5), (1 / 10000, 0.1))  # with higher bounds for the guesses
    ## function used for global fitting
    # results = optimize.basinhopping(MLE, guess, minimizer_kwargs={'method':'nelder-mead'}, niter=20, T=1, stepsize=0.01)
    ## common function used for minimization
    return optimize.minimize(MLE, guess, method='L-BFGS-B', bounds=bnds)


def plot_exp2(read_params=0):
    fnames = tkfd.askopenfilenames()  # list of file(s)
    for fname in fnames:
        print(fname)
        ## read dwell times from a tab separated text file
        ydata = np.loadtxt(fname, delimiter='\t')

        hd, tl = os.path.split(fname)
        dir_out = hd + '/out/'
        if read_params:
            if os.path.exists(f'{dir_out + tl[:-4]}_exp2_res.txt'):  # if already analyzed before
                print('Fitting results exists!')
                try:
                    res = pd.read_csv(f'{dir_out + tl[:-4]}_exp2_res.txt', delimiter='\t', header=0)
                    bins = int(res['bins'])
                    cutoff_l = float(res['cutoff_l'])
                    cutoff_h = float(res['cutoff_h'])
                except:
                    res = np.loadtxt(f'{dir_out + tl[:-4]}_exp2_res.txt')
                    bins = int(res[1003][0])
                    cutoff_l = res[1003][1]
                    cutoff_h = res[1003][2]

            elif os.path.exists(f'{dir_out + tl[:-4]}_exp1_res.txt'):
                print('Fitting results exists for Exp1 fit!')
                try:
                    res = pd.read_csv(f'{dir_out + tl[:-4]}_exp1_res.txt', delimiter='\t', header=0)
                    bins = int(res['bins'])
                    cutoff_l = float(res['cutoff_l'])
                    cutoff_h = float(res['cutoff_h'])
                except:
                    res = np.loadtxt(f'{dir_out + tl[:-4]}_exp1_res.txt')
                    bins = int(res[1003])
                    cutoff_l = res[1004]
                    cutoff_h = res[1005]
                    print('Old model result found.')
            else:  # provide the parameters for new analysis
                print('Unable to read parameters! Will use default parameters.')
                bins = 12
                cutoff_l = 0
                cutoff_h = 10000

        else:  # provide the parameters for new analysis
            print('Using default parameters!')
            bins = 11
            cutoff_l = 0.
            cutoff_h = 5500

        # ydata = np.loadtxt('open_states.csv', delimiter='\t')

        cut_dwelltimes, errors, bin_centres, counts, res, res_bs = create_barplot_data(ydata, N=bins,
                                                                                       cutoff_low=cutoff_l,
                                                                                       cutoff_high=cutoff_h)

        res_errs = np.std(res_bs, axis=0)  # sd along the column
        print('Errors:', res_errs)

        x = np.linspace(bin_centres[0], bin_centres[-1], num=200)
        y_fitted = (1 - res[0]) * res[1] * np.exp(-res[1] * x) + res[0] * res[2] * np.exp(-res[2] * x)
        # y_fitted = (1 - A_all[0]) * K1_all[0] * np.exp(-K1_all[0] * x) + A_all[0] * K2_all[0] * np.exp(-K2_all[0] * x)

        ## plot actual data
        fig, ax = plt.subplots(constrained_layout=True)
        ax.errorbar(bin_centres, counts, fmt='o', mfc='none', color='gray', yerr=2 * errors, capsize=2, capthick=1,
                    lw=1, zorder=1)  # 2*errors = 95% CI
        ax.set_xlabel("Dwell time (s)")
        ax.set_ylabel("Prob. dens.")

        ## plot fitted data on original data
        ax.plot(x, y_fitted, c='r', linestyle='dashed', label="Exp2 MLE fit", zorder=2, lw=1.5)
        # plt.legend()
        ax.set_xscale('log')
        ax.set_yscale('log')
        plt.show()

        # save results and figures in the same directory if 'Yes'
        choice = tkmsg.askquestion('Yes/No', 'Want to save?', icon='warning')
        if choice == 'yes':
            if os.path.exists(dir_out):
                fname = dir_out + tl
            else:
                os.mkdir(dir_out)
                fname = dir_out + tl

            df_res = pd.DataFrame({'k1': [res[1]], 'k1_sd': res_errs[1],  # fist value needs to be a list
                                   'k2': res[2], 'k2_sd': res_errs[2],
                                   'p2': res[0], 'p2_sd': res_errs[0],
                                   'bins': bins, 'cutoff_l': cutoff_l, 'cutoff_h': cutoff_h})
            df_res.to_csv(f'{fname[:-4]}_exp2_res.txt', sep='\t', header=True, float_format='%.6f', index=False)
            np.savetxt(f'{fname[:-4]}_exp2_bs.txt', res_bs, fmt='%0.6f', delimiter='\t')  # %f saves in normal decimal

            if len(ydata) != len(cut_dwelltimes):
                np.savetxt(f'{fname[:-4]}_exp2_cut.txt', np.sort(cut_dwelltimes), fmt='%f',
                           delimiter='\t')  # %f saves in normal decimal
            fig.savefig(f'{fname[:-4]}_exp2_plot.pdf')
            # fig.savefig(f'{fname[:-4]}_exp2plot.png')


if __name__ == '__main__':
    choice = tkmsg.askquestion('Yes/No', 'Want to use old range?', icon='warning')
    if choice == 'yes':
        plot_exp2(read_params=1)
    else:
        plot_exp2(read_params=0)
