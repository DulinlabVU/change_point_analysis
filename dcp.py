""" See about section at the end for more details...
taken from: https://matplotlib.org/2.0.2/examples/user_interfaces/embedding_in_qt5.html
modified by Subhas Ch Bera
Date: 2019-06-26
"""
import os
import sys
import pickle
import pandas as pd
import numpy as np
import scipy.signal as signal

try:
    import ruptures as rpt
except ImportError:
    print('Missing package "ruptures"')
# from drift_corr import linear_drift_corr, poly_drift_corr_by_parts

import matplotlib as mpl
from matplotlib.figure import Figure
from matplotlib.backend_bases import key_press_handler
from matplotlib.backends.backend_qt5agg import (
    FigureCanvasQTAgg as FigureCanvas,
    NavigationToolbar2QT as NavigationToolbar)

try:
    from PyQt5.QtCore import Qt
except ImportError:
    print('Missing package "PyQt5"')
from PyQt5.QtGui import QIcon, QIntValidator
from PyQt5.QtWidgets import QMainWindow, QFileDialog, QApplication, QMessageBox, QStatusBar, QStyleFactory
from PyQt5.uic import loadUi

from modules.read_data import *
from modules.drift_corr import *
from modules.functions import filter_data, set_xylimits


def error_msg(txt):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Critical)
    msg.setText(str(txt))  # str to avoid Value error as it takes str only?!
    # msg.setInformativeText(txt)
    msg.setWindowTitle("Error!")
    msg.exec_()


def warn_msg(txt):
    msg = QMessageBox()
    msg.setIcon(QMessageBox.Warning)
    msg.setText(str(txt))
    msg.setWindowTitle("Warning!")
    msg.exec_()


### the main window:
class MainWin(QMainWindow):
    """
    This creates the main window and embed matplot canvas for plotting. Also contains many functions.
    """

    def __init__(self, parent=None):
        super(MainWin, self).__init__(parent)  # modified from below
        loadUi('dcp.ui', self)
        # loadUi('Qt_stable.ui', self)
        self.setWindowTitle('Detect change points')  # set the GUI title
        self.setWindowIcon(QIcon('logo.png'))  # the GUI logo
        QApplication.setStyle(QStyleFactory.create('Fusion'))  # GUI appearance style
        # self.setWindowFlags(self.windowFlags() | Qt.CustomizeWindowHint)  # to disable the CLOSE (X) button
        # self.setWindowFlags(self.windowFlags() & ~Qt.WindowCloseButtonHint)  # to disable the CLOSE (X) button

        # app.aboutToQuit.connect(self.close_application)
        #####################################
        ###### Creating the plot area #######
        #####################################
        self.fig = Figure(tight_layout=True)  # auto size
        # self.fig = Figure((3.0, 2.0), dpi=100) # fixed size in inch
        self.canvas = FigureCanvas(self.fig)
        # self.canvas.setParent(self.MainArea)
        self.canvas.setFocusPolicy(Qt.StrongFocus)  # this is to avail the key press event of mpl
        # self.canvas.setFocus()
        self.mpl_toolbar = NavigationToolbar(self.canvas, self.MainArea)  # create the mpl toolbar
        # self.setCentralWidget(self.MainArea)
        self.canvas.mpl_connect('button_press_event', self.on_click)  # mpl mouse button events
        self.canvas.mpl_connect('key_press_event', self.on_key_press)  # mpl keypress events

        # set plot params as default---
        mpl.rcParams['agg.path.chunksize'] = 1000000  # smaller value creates point overlapping in plot for large data
        # mpl.rcParams['axes.linewidth'] = 1  # set the value globally
        # mpl.rcParams['axes.labelweight'] = 'bold'  # set the value globally
        mpl.rcParams['font.sans-serif'] = 'Arial'  # set the value globally
        mpl.rcParams['font.size'] = 12  # set the value globally
        # mpl.rcParams['font.weight'] = 'bold'  # set the value globally
        mpl.rcParams['xtick.direction'] = 'in'  # set the value globally
        mpl.rcParams['xtick.labelsize'] = 12  # set the value globally
        # mpl.rcParams['xtick.major.width'] = 1  # set the value globally
        mpl.rcParams['ytick.direction'] = 'in'  # set the value globally
        mpl.rcParams['ytick.labelsize'] = 12  # set the value globally
        # mpl.rcParams['ytick.major.width'] = 1  # set the value globally
        # mpl.rcParams['ytick.minor.width'] = 1  # set the value globally
        # mpl.rcParams['ytick.minor.size'] = 2  # set the value globally
        mpl.rcParams['ytick.minor.left'] = True  # set the value globally
        mpl.rcParams['pdf.fonttype'] = 42  # enables editable test in Illustrator if saved as PDF
        mpl.rcParams['svg.fonttype'] = 'none'  # this works for font edit in AI
        mpl.rcParams['xtick.minor.visible'] = True  # set the value globally
        mpl.rcParams['ytick.minor.visible'] = True  # set the value globally

        # print(mpl.rcParams.keys())
        ###### Adding mpl toolbar and canvas to Layout ######
        self.Frame1VLayout.addWidget(self.mpl_toolbar)
        self.Frame1VLayout.addWidget(self.canvas)

        ###################################
        ######### Menu items ##############
        ###################################
        ## to open file ##
        self.actionOpen.setShortcut('Ctrl+O')  # assigning short cut
        self.actionOpen.setStatusTip('Open file')  # This will show in status bar
        self.actionOpen.triggered.connect(self.open_file)  # perform the action by calling the function.
        ## to select file ##
        # self.actionSelect.triggered.connect(self.select_data)
        ## to clear current data ##
        self.actionClear.triggered.connect(self.init_params)
        ## to save file ##
        self.actionSave.triggered.connect(self.save_file)
        ## to quite ##
        self.actionExit.triggered.connect(self.closeEvent)
        self.actionAbout.triggered.connect(self.about)

        ## https://stackoverflow.com/questions/19646185/python-qt-updating-status-bar-during-program-execution
        self.statusbar1 = QStatusBar()
        self.statusbar2 = QStatusBar()
        self.statusbar.addPermanentWidget(self.statusbar1)  # this align to the right of stat.bar
        self.statusbar.addPermanentWidget(self.statusbar2)  # this align to the right of stat.bar1
        self.statusbar.showMessage('Ready!')
        self.statusbar1.showMessage('Ready!')
        self.statusbar2.showMessage('Ready!')

        ########### Button actions #############
        self.OpenBtn.clicked.connect(self.open_file)
        self.SaveDataBtn.clicked.connect(self.save_file)
        self.SaveFigBtn.clicked.connect(self.save_fig)
        self.PlotPrevBtn.clicked.connect(self.plot_prev)
        self.PlotNextBtn.clicked.connect(self.plot_next)
        self.PlotRefreshBtn.clicked.connect(self.call_plots)
        self.SelectBtn.clicked.connect(self.select_data)
        self.CorDriftBtn.clicked.connect(self.drift_cor)
        self.CPABtn.clicked.connect(self.CPA)
        self.CPABatchBtn.clicked.connect(self.CPA_batch)
        self.DelCPABtn.clicked.connect(self.del_CPA)
        self.DelBrkPtBtn.clicked.connect(self.del_brk)
        self.UndoDelBrkBtn.clicked.connect(self.undo_del_brk)

        ### Line edits ###
        self.setPlotNo.setValidator(QIntValidator())
        self.setPlotNo.editingFinished.connect(self.set_plot_no)
        self.setYlim.editingFinished.connect(self.set_ylim)
        self.setXlim.editingFinished.connect(self.set_xlim)
        self.setFreq.editingFinished.connect(self.set_freq)
        self.setPenVal.editingFinished.connect(self.set_penval)
        self.setDecim.editingFinished.connect(self.set_dec)
        self.setSections.editingFinished.connect(self.get_sec)
        self.setJump.editingFinished.connect(self.set_jump)
        self.setRef.valueChanged.connect(self.set_refval)

        ### Check boxes ###
        self.PlotXYcheckBox.clicked.connect(self.call_plots)
        self.SubRefcheckBox.toggled.connect(self.set_ref)
        self.ZerocheckBox.toggled.connect(self.call_plots)
        self.FiltercheckBox.clicked.connect(self.set_freq)
        self.DecimcheckBox.toggled.connect(self.setDecim.setEnabled)  # TODO: check this uses
        self.DecimcheckBox.toggled.connect(self.set_dec)
        self.DCPGBox.toggled.connect(self.dcp_check)
        self.sectionsCBox.toggled.connect(self.enable_sec)
        self.PlotCalibcheckBox.toggled.connect(self.call_plots)

        ### Combo Box ####
        ## the search methods
        self.SearchMethcomboBox.addItem("Bottom-up Segmentation")  # BottomUp
        self.SearchMethcomboBox.addItem("Binary Segmentation")  # BinSeg
        self.SearchMethcomboBox.addItem("Pelt")  # Pelt
        # self.algo.addItem("Window Based")  #Window # need to know nr of true bkps see Dynp
        self.SearchMethcomboBox.activated[str].connect(self.algorithm_clicked)

        ### The cost functions
        # here a float number is used. If a precise calculation is wanted, either calculate beforehand or change code
        self.CostFunccomboBox.addItem("Least Absolute Deviation")
        self.CostFunccomboBox.addItem("Least Squared Deviation")
        self.CostFunccomboBox.addItem("Gaussian Process Change")
        self.CostFunccomboBox.activated[str].connect(self.method_clicked)

        self.init_params()  # initializing some basic parameters, see func below

        self.list_algos = [  # rpt.Dynp,
            rpt.BottomUp, rpt.Binseg, rpt.Pelt  # , rpt.Window
        ]
        self.list_model = ["l1", "l2", "normal"]  # , "rbf", "linear", "ar", "mahalanobis"]

    def method_clicked(self):
        """selects between different cost functions"""
        self.method_no = self.CostFunccomboBox.currentIndex()
        # print(self.list_model[self.method_no])

    def algorithm_clicked(self):
        """selects between different serach methods"""
        self.algo_no = self.SearchMethcomboBox.currentIndex()
        # print(self.list_algos[self.algo_no])

    def init_params(self):
        """ initializes/resets the variables upon calling """
        # self.fig.clear()
        self.data_status = 0  # False
        self.file_no = 1
        self.data_dict = {}
        self.plot_no = 0
        self.plot_total = 0
        self.save_data = {}
        self.save_data_CPA = {}
        self.cutoff_hz = 1  # 0.5 or 1
        self.acq_freq = 58  # Hz
        self.method_no = 0
        self.algo_no = 0
        self.dec_fac = 10
        self.penvalue = 5
        self.jump = 5
        self.zero_rot = 0
        self.fileout = 0
        self.HighlightZero = False
        self.secs = False
        self.xlimits = (None, None)
        self.ylimits = (None, None)

    def dcp_check(self):
        """manage different checkbox settings"""
        if self.DCPGBox.isChecked() and self.data_status:
            self.get_sec()
            self.SubRefcheckBox.setChecked(True)
            self.SubRefcheckBox.setEnabled(False)
            self.FiltercheckBox.setChecked(False)
            self.FiltercheckBox.setEnabled(False)
            self.ZoomcheckBox.setEnabled(True)
            # self.warn_msg_cpa()
        else:
            self.DCPGBox.setChecked(False)
            self.SubRefcheckBox.setEnabled(True)
            self.FiltercheckBox.setEnabled(True)
            self.ZoomcheckBox.setEnabled(False)
            self.call_plots()

    def enable_sec(self):
        """ enable inputs for sections"""
        if self.sectionsCBox.isChecked():
            self.setSections.setEnabled(True)
            self.setSections.setFocus()
        else:
            self.secs = False
            self.setSections.setEnabled(False)

    def set_jump(self):
        """ used to set jump size, a parameter used in ruptures"""
        try:
            self.jump = int(self.setJump.text())
        except:
            error_msg('Wrong input!')

    def set_zero(self):
        """indicates the zero turn/twist location on the calibration trace"""
        try:
            self.zero_rot = int(self.setSections.text().split(',')[2])
            self.HighlightZero = True
            self.call_plots()
        except:
            self.HighlightZero = False
            self.call_plots()
            # self.error_msg('Wrong input!')

    def set_penval(self):
        """ used to set penalty value, a parameter used in ruptures"""
        try:
            self.penvalue = float(self.setPenVal.text())
            # print(self.penvalue)
        except ValueError:
            pass

    def set_dec(self):
        """Enable decimation input and disable filtering if decimate checkbox is checked"""
        if self.DecimcheckBox.isChecked():
            self.FiltercheckBox.setChecked(False)
            self.FiltercheckBox.setEnabled(False)
            try:
                self.dec_fac = int(self.setDecim.text())
                self.call_plots()
            except ValueError:
                self.self.setDecim.setText('{}'.format(self.dec_fac))
        else:
            self.FiltercheckBox.setEnabled(True)
            self.call_plots()

    def set_ref(self):
        """manage check box setting if reference subtraction check box is toggled"""
        if self.SubRefcheckBox.isChecked() and self.data_status:
            self.setRef.setEnabled(True)
            self.ZerocheckBox.setEnabled(True)
            self.setRef.setMaximum(self.plot_total)
            self.setRef.setValue(self.plot_total)
            warn_msg('Last trace as default ref! Choose to change')
        else:
            self.SubRefcheckBox.setChecked(False)
            self.setRef.setEnabled(False)
            self.ZerocheckBox.setEnabled(False)
        # self.call_plots()
        self.set_refval()

    def set_refval(self):
        """sets the reference number used for data drift correction"""
        self.ref_no = int(self.setRef.value()) - 1  # -1 to adjust python indexing from user input
        self.call_plots()

    def set_plot_no(self):
        """sets the plot number user wants to see. shows error if the number is out of range"""
        try:
            self.plot_no = int(self.setPlotNo.text()) - 1
            # plot number should be 1 to total number of traces/
            if self.plot_no + 1 > self.plot_total or self.plot_no < 0:
                error_msg(f'Plot no. should be within 1 and {self.plot_total}')
            else:
                self.call_plots()
        except ValueError:
            pass

    def set_ylim(self):
        """sets y limits of the plot"""
        self.ylimits = set_xylimits(self.setYlim.text())
        self.fig.gca().set_ylim(self.ylimits)
        self.canvas.draw()
        # self.call_plots()

    def set_xlim(self):
        """sets x limits of the plot"""
        self.xlimits = set_xylimits(self.setXlim.text())
        self.fig.gca().set_xlim(self.xlimits)
        self.canvas.draw()
        # self.call_plots()

    def set_freq(self):
        """ sets the cutoff frequency of the filter if used"""
        if self.FiltercheckBox.isChecked() and self.data_status:
            self.setFreq.setEnabled(True)
            try:
                self.cutoff_hz = float(self.setFreq.text())
                self.call_plots()
            except ValueError:
                self.setFreq.setText('{}'.format(self.cutoff_hz))
                self.call_plots()
        else:
            self.FiltercheckBox.setChecked(False)
            self.setFreq.setEnabled(False)
            self.call_plots()

    def set_drift(self):
        """these parameters used for drift correction if required after reference subtraction.
        division: in how many parts the whole trace user wants to divide
        Polyfit order: the order of the polynomial that will be used to fit those above parts
        Note: these are only used for non-linear drift correction"""
        try:
            self.div = int(self.setDivDrift.text())
            self.order = int(self.setOrderDrift.text())
        except Exception as e:
            error_msg(e)

    def get_sec(self):
        """separate section/locations according to the section file provided"""
        if self.sectionsCBox.isChecked():
            try:
                secs = self.setSections.text().split(',')
                calib, data = secs[:2]
                cal_s = int(calib.split('-')[0])  # as action starts at beginning of a section and
                cal_e = int(calib.split('-')[1]) + 1  # as action ends before previous section.
                d_s = int(data.split('-')[0])

                try:
                    self.calib_start = self.sec_pos[cal_s]
                    self.calib_end = self.sec_pos[cal_e]
                    self.data_sec_start = self.sec_pos[d_s]
                except:
                    self.read_secs()  # reading the sections
                    self.calib_start = self.sec_pos[cal_s]
                    self.calib_end = self.sec_pos[cal_e]
                    self.data_sec_start = self.sec_pos[d_s]

                try:
                    if len(data.split('-')) > 1:
                        d_e = int(data.split('-')[1])
                    else:
                        d_e = d_s + 1
                    self.data_sec_end = self.sec_pos[d_e]
                except:
                    self.data_sec_end = None
                    warn_msg('End section of data is not found.'
                             ' Trace data will be taken up to the END of the experiment!')
                self.relevant_sections = [cal_s, cal_e, d_s]
                self.calib_secs = range(cal_s, cal_e, 1)  # as range exclude the end point and only for calibration part
                self.secs = True
                self.set_zero()
            except Exception as err:
                self.secs = False
                # self.setSections.setEnabled(False)
                self.sectionsCBox.setChecked(False)
                self.call_plots()
                error_msg('Section error: {}'.format(err))
        else:
            self.setSections.setEnabled(False)
            self.sec_pos = None
            self.relevant_sections = None
            self.call_plots()

    def call_plots(self):
        """includes all possible plot methods"""
        if self.data_status:
            try:
                self.plot_data()
                self.update_progressBar()
            except Exception as err:
                # raise
                error_msg(f'Plot error: {err}')
        else:
            self.PlotXYcheckBox.setChecked(False)
            pass

    def open_file(self):
        # TODO: implement recalling last accessed file path
        try:
            filename, _ = QFileDialog.getOpenFileName(self, 'Open File', self.filein,
                                                      '*.txt *.csv', options=QFileDialog.DontUseNativeDialog)
        except:
            filename, _ = QFileDialog.getOpenFileName(self, 'Open File',
                                                      '*.txt *.csv', options=QFileDialog.DontUseNativeDialog)
        self.filein = filename
        print(filename)
        if filename:
            if self.plot_total != 0:
                if self.msg_box('Want to clear old data?') == QMessageBox.Yes:
                    self.init_params()
            self.data = read_mt_data(self.filein)  # read data with header names
            self.extract_data(self.data)
            self.check_data()
            self.call_plots()
        else:
            pass

    def extract_data(self, data):
        """ Reads named data with x, y and z or only z data and creates data to be used for plotting."""
        self.statusbar2.showMessage('Extracting data...')
        try:
            times, self.names_z, self.names_x, self.names_y = cols_names(data)

            if len(self.names_x) == 0 or len(self.names_y) == 0:
                warn_msg('Only Z data found!')
                self.PlotXYcheckBox.setEnabled(False)

            time = data[times[0]]  # get the 'time' column
            self.time = time - time.values[0]  # subtracting the first frame time to make it 0.

            self.plot_total = len(self.names_z)
            self.ref_no = self.plot_total - 1  # as python indexing starts from 0 not from 1

            if self.SubRefcheckBox.isChecked():
                self.setRef.setValue(self.plot_total)
                warn_msg('Last trace as default ref! Change if needed.')
            self.statusbar2.showMessage(f'Total no of dataset: {self.plot_total}')
        except AttributeError as err:
            error_msg(f'Data extraction error: {err}')

    def check_data(self):
        try:
            if self.filein or self.plot_total > 0:
                self.data_status = 1
        except:
            error_msg('No data found! Please choose data first.')

    def read_secs(self):
        """reads the section file that contains position/location of sections"""
        try:
            self.sec_data = read_mt_data(f'{self.filein[:-4]}_sections.txt', name_header=0)
            self.sec_pos = self.sec_data.iloc[:, 1]
        except FileNotFoundError:
            print('Unable to find section file. Choose manually.')
            name, _ = QFileDialog.getOpenFileName(self, 'Select section file', self.filein)
            self.sec_data = read_mt_data(name, name_header=0)
            # self.sec_data = read_mt_data(name, name_header=False)
            self.sec_pos = self.sec_data.iloc[:, 1]

    def plot_next(self):
        """display next plot if clicked once"""
        if self.data_status:
            if self.ZoomcheckBox.isChecked():
                left, right = self.ax1.get_xlim()
                x_dif = right - left
                self.ax1.set_xlim((left + x_dif), (right + x_dif))
                self.canvas.draw()
            elif self.plot_no + 1 >= self.plot_total:
                if self.msg_box('This was the last dataset! Want to plot from the START?') == QMessageBox.Yes:
                    self.plot_no = 0
                    self.call_plots()
            else:
                self.plot_no += 1
                # plot is called upon with updated ii_plot, such that you will plot the next column in the dataset
                self.call_plots()

    def plot_prev(self):
        """display previous plot is clicked once"""
        if self.data_status:
            if self.ZoomcheckBox.isChecked():
                left, right = self.ax1.get_xlim()
                x_dif = right - left
                self.ax1.set_xlim((left - x_dif), (right - x_dif))
                self.canvas.draw()
            elif self.plot_no - 1 < 0:
                if self.msg_box('This was the first dataset! Want to plot from the END?') == QMessageBox.Yes:
                    self.plot_no = self.plot_total - 1
                    self.call_plots()
            else:
                self.plot_no -= 1
                # plot is called upon with updated plot_no, such that you will plot the next column in the dataset
                self.call_plots()

    def drift_cor(self):
        ax = self.ax1
        # TODO: fix plot error if drift corrected after cutting (double mouse click) the trace
        if self.LinDriftcheckBox.isChecked() and self.NonLinDriftGBox.isChecked():
            error_msg('Please choose only one option!')
            return
        try:
            if self.LinDriftcheckBox.isChecked():
                self.trace_data = linear_drift_corr(self.trace_data)[0]
                # self.dec_trace = linear_drift_corr(self.dec_trace)[0]
                ax.plot(np.arange(len(self.trace_data)) / 58, self.trace_data, alpha=0.5)
                self.canvas.draw()

            if self.NonLinDriftGBox.isChecked():
                self.set_drift()
                self.trace_data = poly_drift_corr_by_parts(self.trace_data, self.div, self.order)[0]
                # self.dec_trace = poly_drift_corr_by_parts(self.dec_trace, self.div, self.ordr)[0]
                ax.plot(np.arange(len(self.trace_data)) / 58, self.trace_data, alpha=0.5)
                self.canvas.draw()

            if self.msg_box('Accept drift correction?') == QMessageBox.Yes:
                self.save_data[self.names_z[self.plot_no]] = self.trace_data
                self.call_plots()
        except Exception as err:
            error_msg(err)

    def plot_data(self):
        yz_data = self.data[self.names_z[self.plot_no]]
        if self.PlotXYcheckBox.isChecked():
            yx_data = self.data[self.names_x[self.plot_no]]
            yy_data = self.data[self.names_y[self.plot_no]]

        if self.SubRefcheckBox.isChecked() or self.DCPGBox.isChecked():
            yz_data = yz_data.sub(self.data[self.names_z[self.ref_no]], axis=0)
            self.original_data = yz_data  # ref subtracted
            self.original_time = self.time
            if self.ZerocheckBox.isChecked():  # making the base line '0'
                try:
                    # get the minimum of the trace from the max extension (zero force) region
                    data_min = yz_data[self.sec_pos[1]: self.sec_pos[2]].min()
                    data_max = yz_data[self.sec_pos[1]: self.sec_pos[2]].max()
                except:
                    # TODO: instead of using whole trace, use 1/3rd of the trace from beginning to find min/max
                    data_min = yz_data.min()  # gets the minimum from the whole trace
                    data_max = yz_data.max()  # gets the maximum from the whole trace
                yz_data = yz_data - data_min
                extn = (data_max - data_min)  # dsDNA bp length
                length = extn / 0.34  # dsDNA bp length
                self.dispExtn.setText(f'{round(extn, 2)} µm, {round(length, 2)} kb')

            if self.PlotXYcheckBox.isChecked():
                yx_data = yx_data.sub(self.data[self.names_x[self.ref_no]], axis=0)  # subtract ref bead
                yy_data = yy_data.sub(self.data[self.names_y[self.ref_no]], axis=0)

            elif self.DCPGBox.isChecked() and self.secs:  # ref sub is compulsory
                self.calib_data = yz_data.iloc[self.calib_start:self.calib_end].values
                self.calib_time = (
                        self.time.iloc[self.calib_start:self.calib_end] - self.time.iloc[self.calib_start]).values
                if self.names_z[self.plot_no] in self.save_data:
                    self.trace_data = self.save_data[self.names_z[self.plot_no]]
                else:
                    self.trace_data = yz_data.iloc[self.data_sec_start:self.data_sec_end].values
                self.trace_time = (self.time.iloc[self.data_sec_start:self.data_sec_end] - self.time.iloc[
                    self.data_sec_start]).values
                self.calib_dict = {}
                for i in self.calib_secs:
                    t = yz_data.iloc[self.sec_pos[i]:self.sec_pos[i + 1]].values
                    self.calib_dict[i] = t  # as i is the section no
                self.zero_pos_x = (self.time.iloc[self.sec_pos[self.zero_rot]] - self.time.iloc[
                    self.calib_start]) / 1000
                self.zero_pos_y = (self.time.iloc[self.sec_pos[self.zero_rot + 1]] - self.time.iloc[
                    self.calib_start]) / 1000

            elif self.DCPGBox.isChecked() and not self.secs:  # ref sub is compulsory
                if self.names_z[self.plot_no] in self.save_data:
                    self.trace_data = self.save_data[self.names_z[self.plot_no]]
                else:
                    self.trace_data = yz_data.values
                self.trace_time = self.time.values
                self.calib_dict = {}
                self.calib_data = np.array([0])
                self.calib_time = np.array([0])

        if self.FiltercheckBox.isChecked():
            # self.init_filter_params()
            _, yz_data_fltr = filter_data(yz_data, self.acq_freq, self.cutoff_hz, filter_type='filtfilt')
            if self.PlotXYcheckBox.isChecked():
                _, yx_data_fltr = filter_data(yx_data, self.acq_freq, self.cutoff_hz, filter_type='filtfilt')
                _, yy_data_fltr = filter_data(yy_data, self.acq_freq, self.cutoff_hz, filter_type='filtfilt')
            elif self.DCPGBox.isChecked() and self.secs:
                self.calib_data_fltr = yz_data_fltr[self.calib_start:self.calib_end]
                self.trace_data_fltr = yz_data_fltr[self.data_sec_start:self.data_sec_end]
                if self.names_z[self.plot_no] in self.save_data:
                    self.trace_data_fltr = signal.filtfilt(self.taps, 1.0, self.trace_data)
            elif self.DCPGBox.isChecked() and not self.secs:
                self.trace_data_fltr = yz_data_fltr[self.data_sec_start:self.data_sec_end]
                if self.names_z[self.plot_no] in self.save_data:
                    self.trace_data_fltr = signal.filtfilt(self.taps, 1.0, self.trace_data)

        if not self.OverlaycheckBox.isChecked() or self.PlotXYcheckBox.isChecked():
            self.fig.clear()
        if self.PlotXYcheckBox.isChecked():  # TODO: add XY overlay option
            if self.OverlaycheckBox.isChecked():
                warn_msg('Overlay only available for Z-data not X/Y!')
            axs = self.fig.subplots(3, 1, sharex=True)
            ax3 = axs[0]
            ax2 = axs[1]
            self.ax1 = ax1 = axs[2]
            ax3.plot(self.time / 1000, yx_data, '-', label='Raw data X')
            ax2.plot(self.time / 1000, yy_data, '-', label='Raw data Y')
            ax1.plot(self.time / 1000, yz_data, '-', label='Raw data Z')
            if self.FiltercheckBox.isChecked():
                ax3.plot(self.time / 1000, yx_data_fltr, '-', label='Filter data X')
                ax2.plot(self.time / 1000, yy_data_fltr, '-', label='Filter data Y')
                ax1.plot(self.time / 1000, yz_data_fltr, '-', label='Filter data Z')

            if self.ShowTitlecheckBox.isChecked():
                ax3.set_title(f'Plot # {self.plot_no + 1} of {self.plot_total}')
            ax2.set_ylabel('Y pos ($\mu$m)')
            ax3.set_ylabel('X pos ($\mu$m)')
            if self.ShowLegendcheckBox.isChecked():
                ax2.legend(loc='upper right')
                ax3.legend(loc='upper right')
        elif self.DCPGBox.isChecked():
            if self.names_z[self.plot_no] in self.save_data_CPA.keys():  # reading already analysed data
                self.trace_data = self.save_data_CPA[self.names_z[self.plot_no]]['raw_data_cut'][2]
                self.trace_time = self.save_data_CPA[self.names_z[self.plot_no]]['raw_data_cut'][3]
                self.dec_trace = self.save_data_CPA[self.names_z[self.plot_no]]['decimated_trace'][0]
                self.dec_time = self.save_data_CPA[self.names_z[self.plot_no]]['decimated_trace'][1]
                self.my_bkps = self.save_data_CPA[self.names_z[self.plot_no]]['break_points']
                self.plot_cpa()
            else:
                if self.secs:
                    self.ax1 = self.fig.add_subplot(2, 1, 2)
                    ax2 = self.fig.add_subplot(2, 1, 1)
                    ax2.plot(self.calib_time / 1000, self.calib_data, 'k-', alpha=0.5, label='Calibration raw data')
                    if self.ShowTitlecheckBox.isChecked():
                        ax2.set_title(f'Plot of dataset no. {self.plot_no + 1} of {self.plot_total}')
                    if self.ShowLegendcheckBox.isChecked():
                        ax2.legend(loc='upper right')

                    if self.HighlightZero:
                        ax2.axvspan(self.zero_pos_x, self.zero_pos_y, facecolor='r', alpha=0.3, label='magrot 0')
                else:
                    self.ax1 = self.fig.add_subplot()
                    if self.PlotCalibcheckBox.isChecked():
                        warn_msg('Calibration data not found!')
                    if self.ShowTitlecheckBox.isChecked():
                        self.ax1.set_title(f'Plot of dataset no. {self.plot_no + 1} of {self.plot_total}')
                self.ax1.plot(self.trace_time[:len(self.trace_data)] / 1000, self.trace_data, 'k-', alpha=0.5,
                              label='Raw data', linewidth=0.5)

                # self.ax1.plot(self.trace_time/1000, self.trace_data, 'k-', alpha=0.5, label='Raw data', linewidth=0.5)
                if self.DecimcheckBox.isChecked():
                    self.dec_trace = self.aver_data(self.trace_data, self.dec_fac)
                    self.dec_time = self.aver_data(self.trace_time, self.dec_fac)
                    if self.secs:
                        self.dec_calib = self.aver_data(self.calib_data, self.dec_fac)
                        self.dec_calib_time = self.aver_data(self.calib_time, self.dec_fac)
                        ax2.plot(self.dec_calib_time / 1000, self.dec_calib, 'k-', alpha=0.8,
                                 label='Dec data')
                        if self.ShowLegendcheckBox.isChecked():
                            ax2.legend(loc='upper right')

                    else:
                        self.dec_calib = np.array([0])
                        self.dec_calib_time = np.array([])
                    self.ax1.plot(self.dec_time / 1000, self.dec_trace, 'k-', alpha=0.7,
                                  label='Dec data', linewidth=0.8)
                    # self.ax1.plot(np.arange(len(self.dec_trace)) / 58 * self.dec_fac, self.dec_trace, 'k-', alpha=0.7,
                    #               label='Dec data', linewidth=0.8)
                if self.FiltercheckBox.isChecked() and not self.DecimcheckBox.isChecked():
                    if self.secs:
                        ax2.plot(self.calib_time / 1000, self.calib_data_fltr, '-', color='black',
                                 label=f'Filtered at {self.cutoff_hz} Hz')
                    self.ax1.plot(self.trace_time[:len(self.trace_data)] / 1000, self.trace_data_fltr, '-',
                                  color='black', label=f'Filtered at {self.cutoff_hz} Hz')
                    if self.secs:
                        if self.ShowTitlecheckBox.isChecked():
                            ax2.set_title(
                                f'Plot # {self.plot_no + 1} ({self.names_z[self.plot_no][:-3]}) of {self.plot_total}')
                        ax2.set_ylabel('Z pos (µm)')
                        if self.ShowLegendcheckBox.isChecked():
                            ax2.legend(loc='lower center')
        else:
            # self.ax1 = self.fig.subplots()  # Does not work for overlay even if figure is not cleared
            self.ax1 = self.fig.add_subplot(1, 1, 1)  # overlay works if figure is not cleared.
            self.ax1.plot(self.time / 1000, yz_data, '-', label='Raw data')
            if self.FiltercheckBox.isChecked():
                self.ax1.plot(self.time / 1000, yz_data_fltr, '-', label=f'Filtered at {self.cutoff_hz} Hz')
            if self.ShowTitlecheckBox.isChecked():
                self.ax1.set_title(f'Plot of dataset no. {self.plot_no + 1} of {self.plot_total}')
        self.ax1.set_ylabel('Z position (µm)')
        self.ax1.set_xlabel('Time (s)')
        self.ax1.set_ylim(self.ylimits)
        self.ax1.set_xlim(self.xlimits)
        if self.ShowLegendcheckBox.isChecked():
            self.ax1.legend(loc='upper right')
        self.canvas.draw()

    def aver_data(self, data, points, t_data=False):
        """ average number of points provided from the data
        input: numpy array
        returns: numpy array """
        av_data = np.array([])
        for i in range(len(data) // points):
            try:
                # TODO: verify shifting of decimated data cf. original data
                av_data = np.append(av_data, data[i * points: (i + 1) * points].mean())
            except:
                pass  # this will discard the end points less than decimation factor
        # if t_data: # to adjust the time shift after averaging
        #     av_data = av_data + data[:points].mean()  # TODO: needs to be tested
        return av_data

    def CPA(self):
        self.statusbar.showMessage('Please wait while detecting change points...')
        self.method = self.list_model[self.method_no]
        if not self.DecimcheckBox.isChecked():
            self.DecimcheckBox.setChecked(True)
        self.algorithm = self.list_algos[self.algo_no](model=self.method, jump=self.jump).fit(
            self.dec_trace)  # only accepts np array!
        print(self.list_algos[self.algo_no])
        print(self.method)
        try:
            self.my_bkps = self.algorithm.predict(pen=self.penvalue)
        except TypeError:
            print('Problem using penalty value!')
            self.my_bkps = self.algorithm.predict()
        print(len(self.my_bkps), self.my_bkps[:10], '...')
        self.plot_cpa()
        self.savedata()

    def CPA_batch(self):
        if self.msg_box('Want to analyze all/selected traces?') == QMessageBox.Yes:
            if len(self.save_data) == 0 and not self.AnalAllCBox.isChecked():
                warn_msg("Please check 'All' to analyze all the data or"
                         " select the dataset(s) first for batch analysis")
            elif self.AnalAllCBox.isChecked():  # analyze all the files
                for name in self.names_z:  # 1st one is time, removed
                    try:
                        self.plot_no = self.names_z.index(name)  # getting the index no from name
                    except:
                        continue
                    self.plot_data()
                    self.method = self.list_model[self.method_no]
                    if not self.DecimcheckBox.isChecked():
                        self.DecimcheckBox.setChecked(True)
                    self.algorithm = self.list_algos[self.algo_no](model=self.method, jump=self.jump).fit(
                        self.dec_trace)  # only accepts np array!
                    print(self.list_algos[self.algo_no])
                    print(self.method)
                    try:
                        self.my_bkps = self.algorithm.predict(pen=self.penvalue)
                    except TypeError:
                        print('Problem using penalty value!')
                        self.my_bkps = self.algorithm.predict()
                    print(len(self.my_bkps), self.my_bkps[:10], '...')
                    self.savedata()
                self.call_plots()
                warn_msg("Analysis completed!")
            else:  # analyze only selected files
                for name in self.save_data:  # 1st one is time, removed
                    try:
                        self.plot_no = self.names_z.index(name)  # getting the index no from name
                    except:
                        continue
                    self.plot_data()
                    self.method = self.list_model[self.method_no]
                    if not self.DecimcheckBox.isChecked():
                        self.DecimcheckBox.setChecked(True)
                    self.algorithm = self.list_algos[self.algo_no](model=self.method, jump=self.jump).fit(
                        self.dec_trace)  # only accepts np array!
                    print(self.list_algos[self.algo_no])
                    print(self.method)
                    try:
                        self.my_bkps = self.algorithm.predict(pen=self.penvalue)
                    except TypeError:
                        print('Problem using penalty value!')
                        self.my_bkps = self.algorithm.predict()
                    print(len(self.my_bkps), self.my_bkps[:10], '...')
                    self.savedata()
                self.call_plots()
                warn_msg("Analysis completed!")
        else:
            pass

    def plot_cpa(self):
        # self.xlimits = self.fig.get_axes()[1].get_xlim()
        self.fig.clf()
        self.ax1 = self.fig.add_subplot(212)
        ax2 = self.fig.add_subplot(211)

        if self.PlotCalibcheckBox.isChecked():

            try:
                ax2.plot(self.calib_time / 1000, self.calib_data, 'k-', alpha=0.5, label='Calibration raw data')
                ax2.plot(self.dec_calib_time / 1000, self.dec_calib, 'k-', alpha=0.8, label='Dec data')
                if self.HighlightZero:
                    ax2.axvspan(self.zero_pos_x, self.zero_pos_y, facecolor='k', alpha=0.3, label='magrot 0')
            except:
                ax2.cla()
                ax2.plot(self.trace_time[:len(self.trace_data)] / 1000, self.trace_data, '-', label='Raw data')
                warn_msg('Unable to plot calibration data! Raw data will be used instead.')
        else:
            ax2.plot(self.trace_time[:len(self.trace_data)] / 1000, self.trace_data, '-', label='Raw data')

        self.ax1.plot(self.trace_time[:len(self.trace_data)] / 1000, self.trace_data, 'k-', alpha=0.5,
                      label='Trace raw data')
        self.ax1.plot(self.dec_time / 1000, self.dec_trace, 'k-', alpha=0.8, label='Dec data')

        if len(self.my_bkps) > 0:
            for i in range(len(self.my_bkps)):
                # as the bkps starts from 0, but the time does not, adding fist time point to adjust.
                bkp = self.my_bkps[i] * (self.dec_fac / 58) + self.dec_time[0] / 1000  # bkps 1 frame shifted to right!?
                self.ax1.axvline(x=bkp, color='m', alpha=0.8)  # vertical line at the bkp
                if self.LabelBkpscheckBox.isChecked():
                    self.ax1.text(bkp, np.mean(self.trace_data), f'{i + 1}', color='r')  # display no. on bkp
        else:
            warn_msg('No break point found!')
        self.ax1.set_xlabel('Time (s)')
        self.ax1.set_ylabel('Z-position ($\mu$m)')
        ax2.set_ylabel('Z-position ($\mu$m)')
        ax2.set_ylim(self.ylimits)
        self.ax1.set_ylim(self.ylimits)
        if self.ZoomcheckBox.isChecked():
            self.ax1.set_xlim(self.xlimits)

        if self.ShowTitlecheckBox.isChecked():
            ax2.set_title(f'Plot of dataset no. {self.plot_no + 1} of {self.plot_total}')

        if self.ShowLegendcheckBox.isChecked():
            self.ax1.legend(loc='upper right')
            ax2.legend(loc='lower center')
        self.canvas.draw()

    def savedata(self):
        # saves the raw data, filtered data - complete and cut in calibration and trace -, the break points and the relevant sections
        current_data = self.names_z[self.plot_no]  # name of data column, a string
        save_dic = {'raw_data_cut': [self.calib_data, self.calib_time, self.trace_data, self.trace_time],
                    'break_points': self.my_bkps,
                    'decimated_trace': [self.dec_trace, self.dec_time, self.dec_calib, self.dec_calib_time],
                    'calibration_turn': self.calib_dict,
                    'section_info': [self.sec_pos, self.relevant_sections],
                    'CPA_info': [self.SearchMethcomboBox.currentText(), self.CostFunccomboBox.currentText(),
                                 self.penvalue, self.dec_fac, self.jump],
                    'original_data': [self.original_data, self.original_time],
                    'Info': "Dict key info: Raw_data_cut-> lists: calibration data, calibration time, trace data, trace time; \
                            decimated_trace-> lists: dec trace, dec time, dec calibration data, dec cal time;\
                            Section info-> lists: All sections, Section locations, Relevant sections;\
                            CPA info-> lists: Method, Cost functions, Penalty value, Dec factor, Jump; break points are the index of ydata, NOT real time values;\
                            calibration_turn-> dict of calibration sections as key with corresponding calibration data."
                    }
        self.save_data_CPA[current_data] = save_dic
        self.ShowFiles.setText(str(list(self.save_data_CPA.keys())))

    def del_CPA(self):
        if self.msg_box('Delete current analysis?') == QMessageBox.Yes:
            k = self.names_z[self.plot_no]
            if k in self.save_data_CPA:
                del self.save_data_CPA[k]
                self.ShowFiles.setText(str(list(self.save_data_CPA.keys())))
                self.statusbar.showMessage('Current analysis successfully deleted!')
                self.call_plots()
            else:
                error_msg('No data found with current file name!')
        else:
            pass

    def del_brk(self):
        lst = self.ReadBrkPt.text().split(',')
        self.xlimits = self.ax1.get_xlim()
        self.ax1.set_xlim(self.xlimits)
        self.my_bkps_old = self.my_bkps[:]  # making a copy with ':'
        while lst:
            indx = lst.pop()
            try:
                no = int(indx) - 1
                if len(self.my_bkps) > no >= 0:
                    del self.my_bkps[no]
                    self.save_data_CPA[self.names_z[self.plot_no]]['break_points'] = self.my_bkps
                else:
                    error_msg('Wrong input!')
            except ValueError as err:
                error_msg(err)
        self.plot_cpa()

    def undo_del_brk(self):
        try:
            self.my_bkps = self.my_bkps_old
            self.save_data_CPA[self.names_z[self.plot_no]]['break_points'] = self.my_bkps
            # self.data[self.beads[self.plot_no]]['break_points'] = self.my_bkps
            self.call_plots()
        except Exception as err:
            warn_msg(err)

    def select_data(self):
        # self.selec_trace = self.selec_trace.append(self.names_z[self.plot_no])
        try:
            if self.time.name not in self.save_data:
                self.save_data[
                    self.time.name] = self.time.copy()  # making a copy as pdf from original data to avoid warning, see below
            else:
                pass

            if self.names_z[self.plot_no] in self.save_data:
                warn_msg('You have already selected this dataset!')
            else:
                if self.PlotXYcheckBox.isChecked():
                    self.save_data[self.names_x[self.plot_no]] = self.data[self.names_x[self.plot_no]]
                    self.save_data[self.names_y[self.plot_no]] = self.data[self.names_y[self.plot_no]]
                    self.save_data[self.names_z[self.plot_no]] = self.data[self.names_z[self.plot_no]]
                else:
                    if self.DCPGBox.isChecked():
                        self.save_data[self.names_z[self.plot_no]] = self.trace_data
                    else:
                        self.save_data[self.names_z[self.plot_no]] = self.data[self.names_z[self.plot_no]]
                    ### warning!!!! if not make a copy of data, see above
                    # A value is trying to be set on a copy of a slice from a DataFrame.
                    # ## Try using .loc[row_indexer,col_indexer] = value instead
            self.statusbar1.showMessage(f'Dataset {self.plot_no + 1} is selected.')
        except Exception as err:
            error_msg(f'Problem in data selection: {err}')

    def save_file(self):
        warn_msg("Please use '.pickle file extension to save analyzed data. "
                 "Or use '.txt' file extension to save raw/not-analyzed data.")
        if self.fileout:
            path = self.fileout
        else:
            path = self.filein

        f_out, _ = QFileDialog.getSaveFileName(self, 'Save file', path, '*.txt *.pickle ')
        self.fileout = f_out
        self.statusbar.showMessage(f'Output file name: {f_out}')

        if f_out == '':
            pass
        elif '.txt' in self.fileout:
            if len(self.save_data) == 0:
                warn_msg('No data to save as ".txt" file! Choose to save as ".pickle" file instead.')
            else:
                try:
                    pdf = pd.DataFrame(self.save_data)
                    pdf.to_csv(path_or_buf=self.fileout, sep='\t', header=True, index=False)
                    if self.secs:
                        self.sec_data.to_csv(f'{self.fileout[:-4]}_sections.txt', sep='\t', header=False, index=False)
                    warn_msg('Data successfully saved as a txt file!')
                except ValueError:
                    error_msg('Problem saving! Most likely different length of data sets has been selected.')
                except Exception as err:
                    error_msg(err)
        elif 'pickle' in self.fileout:
            with open(self.fileout, 'wb') as myfile:
                if self.DCPGBox.isChecked():
                    if len(self.save_data_CPA) == 0:
                        warn_msg('No data to save! Analyze data first OR uncheck "DCP" to save selected traces only.')
                    pickle.dump(self.save_data_CPA, myfile)
                    warn_msg('Analyzed data successfully saved as a pickle file!')
                else:
                    pickle.dump(self.save_data, myfile)
                    warn_msg('Data (NOT analyzed) successfully saved as a pickle file!')
        else:
            error_msg('Unable to save! File formats needs to be either .txt (raw data only)'
                      ' or .pickle (analyzed/raw data)!')

    def save_fig(self):
        if self.fileout:
            path = self.fileout
        else:
            path = self.filein
        f_out, _ = QFileDialog.getSaveFileName(self, 'Save current figure', path,
                                               "Images (*.png *.tiff *.eps *.svg *.jpg *.pdf)")
        self.fileout = f_out
        if f_out == '':
            pass
        else:
            try:
                if 'eps' in f_out:
                    self.fig.savefig(f_out, format='eps')
                elif 'svg' in f_out:
                    self.fig.savefig(f_out, format='svg')
                else:
                    self.fig.savefig(f_out)
                self.statusbar1.showMessage('Figure successfully saved!')
            except Exception as err:
                error_msg(f'Unable to save figure: {err}')

    def update_progressBar(self):
        try:
            done = 100 * (self.plot_no + 1) // self.plot_total  # as plot_no starts at 0.
            # set the value of the progressBar to a % of the total dataset
            self.progressBar.setValue(done)
            self.statusbar1.showMessage('Plot # {} on display'.format(self.plot_no + 1))
        except Exception as err:
            error_msg(f'Error updating progressBar: {err}')

    def on_click(self, event):
        """detects mouse click events. used for cutting the traces in both from the beginning and end based
        on double left click (to cut from the beginning) or right click (to cut from the end)"""
        ax1 = self.fig.gca()
        if event.button == 1 and event.dblclick:  # button 1 for left click
            p = round(event.xdata, 2)
            ax1.cla()
            print('Clicked left mouse button, x:', p)
            indx = (abs(self.trace_time - p * 1000)).argmin()
            self.trace_data = self.trace_data[indx:]
            self.trace_time = self.trace_time[indx:]
            ax1.plot(self.trace_time / 1000, self.trace_data, color='gray', label='Raw trace')

            if self.DecimcheckBox.isChecked():
                indx = (abs(self.dec_time - p * 1000)).argmin()
                self.dec_trace = self.dec_trace[indx:]
                self.dec_time = self.dec_time[indx:]
                ax1.plot(self.dec_time / 1000, self.dec_trace, color='black', label='Dec trace')

            if self.FiltercheckBox.isChecked():
                _, yz_data_fltr = filter_data(self.trace_data, self.acq_freq, self.cutoff_hz, filter_type='filtfilt')
                ax1.plot(self.trace_time / 1000, yz_data_fltr, color='black', label='Dec trace')

            warn_msg('''Double clicked left mouse button at {} in X.
                        Please double click with right mouse button to select
                        the end position. Else, all data to the right will be taken'''.format(p))
            ax1.legend()

        if event.button == 3 and event.dblclick:
            p = round(event.xdata, 2)
            ax1.cla()
            print('Clicked right mouse button, x:', p)
            indx = (abs(self.trace_time - p * 1000)).argmin()
            self.trace_data = self.trace_data[:indx]
            self.trace_time = self.trace_time[:indx]
            ax1.plot(self.trace_time / 1000, self.trace_data, color='gray', label='Raw trace')

            if self.DecimcheckBox.isChecked():
                indx = (abs(self.dec_time - p * 1000)).argmin()
                self.dec_trace = self.dec_trace[:indx]
                self.dec_time = self.dec_time[:indx]
                ax1.plot(self.dec_time / 1000, self.dec_trace, color='black', label='Dec trace')

            if self.FiltercheckBox.isChecked():
                # self.init_filter_params()
                # yz_data_fltr = signal.filtfilt(self.taps, 1.0, self.trace_data)
                _, yz_data_fltr = filter_data(self.trace_data, self.acq_freq, self.cutoff_hz, filter_type='filtfilt')
                ax1.plot(self.trace_time / 1000, yz_data_fltr, color='black', label='Dec trace')

            warn_msg('''Double clicked right mouse button at {} in X.
                        Please double click with left mouse button to select
                        the end position. Else, all data to the left will be taken'''.format(p))
            ax1.legend()
        self.canvas.draw()

    def on_key_press(self, event):
        """detects key board evets.
        left arrow: plot previous data
        right arrow: plot next data"""
        k = event.key
        if k == 'right':
            # print(f'Pressed {k} key.')
            self.plot_next()
        if k == 'left':
            # print(f'Pressed {k} key.')
            self.plot_prev()
        # implement the default mpl key press events described at
        # http://matplotlib.org/users/navigation_toolbar.html#navigation-keyboard-shortcuts
        key_press_handler(event, self.canvas, self.mpl_toolbar)

    def closeEvent(self, event):
        """Handles the window close event if user wants to (or accidentally) click close window"""
        choice = QMessageBox.question(self, 'Message',
                                      "Are you sure to close?", QMessageBox.Yes |
                                      QMessageBox.No, QMessageBox.No)
        if choice == QMessageBox.Yes:
            print('Window closed!')
            event.accept()
        else:
            event.ignore()

    def about(self):
        """infomation about the script"""
        QMessageBox.about(self, "About",
                          """embedding matplot in qt5 an example
        Copyright 2015 BoxControL

        This program is inherited from a simple example of a Qt5 application
        embedding matplotlib canvases. It is base on example from matplolib
        documentation, and initially was developed from Florent Rougon and Darren
        Dale. Modified by Subhas Ch Bera from India while working at IZKF, FAU, Germany.

        https://matplotlib.org/2.0.2/examples/user_interfaces/embedding_in_qt5.html

        It may be used and modified with no restriction; raw copies as well as
        modified versions may be distributed without limitation."""
                          )

    def msg_box(self, msg):
        choice = QMessageBox.question(self, 'Message', msg,
                                      QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
        return choice


def run():
    app = QApplication(sys.argv)
    Gui = MainWin()
    Gui.show()
    sys.exit(app.exec_())


if __name__ == "__main__":
    run()
