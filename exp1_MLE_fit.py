import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib as mpl
from scipy import optimize
import scipy.stats as stats
import tkinter.filedialog as tkfd
import tkinter.messagebox as tkmsg
import os

## formatting the figure look and fonts for saving ##

# mpl.rcParams['figure.figsize'] = [3.54, 2.36]  # in cm it is 9/6 (w/h)
mpl.rcParams['figure.figsize'] = [2.66, 1.75]  # in cm it is 9/6 (w/h)
# mpl.rcParams['figure.figsize'] = [2.3, 1.5]  # in cm it is 9/6 (w/h)
# mpl.rcParams['figure.constrained_layout.use'] = True
# mpl.rcParams['axes.linewidth'] = 1  # set the value globally
mpl.rcParams['axes.labelweight'] = 'bold'  # set the value globally
mpl.rcParams['font.sans-serif'] = 'Arial'  # set the value globally
mpl.rcParams['font.size'] = 12  # set the value globally
mpl.rcParams['font.weight'] = 'bold'  # set the value globally
mpl.rcParams['xtick.top'] = True  # set the value globally
mpl.rcParams['xtick.direction'] = 'in'  # set the value globally
mpl.rcParams['xtick.labelsize'] = 11  # set the value globally
# mpl.rcParams['xtick.labeltop'] = True  # set the value globally
# mpl.rcParams['xtick.labelbottom'] = False  # set the value globally
# mpl.rcParams['xtick.major.width'] = 1  # set the value globally
# mpl.rcParams['xtick.major.size'] = 5  # set the value globally
mpl.rcParams['ytick.right'] = True  # set the value globally
# mpl.rcParams['ytick.labelleft'] = False  # set the value globally
mpl.rcParams['ytick.direction'] = 'in'  # set the value globally
mpl.rcParams['ytick.labelsize'] = 11  # set the value globally
# mpl.rcParams['ytick.major.width'] = 1  # set the value globally
# mpl.rcParams['ytick.major.size'] = 5  # set the value globally
mpl.rcParams['svg.fonttype'] = 'none'  # this works for font edit in AI
mpl.rcParams['pdf.fonttype'] = 42  # this works for font edit in AI
mpl.rcParams['xtick.minor.visible'] = True  # set the value globally
mpl.rcParams['ytick.minor.visible'] = True  # set the value globally


def bootstrap(X, n=None):
    """ Bootstrap resample an array-like
    Parameters
    ----------
    X : array_like
      data to resample
    n : int, optional
      length of re-sampled array, equal to len(X) if n==None
    -------
    returns X_resample
    """
    if n is None:
        n = len(X)
    X_resample = np.random.choice(X, n)
    return X_resample


def create_barplot_data(dwelltime, N=10, cutoff_low=0.5, cutoff_high=5000):
    """
    :param dwelltime: list of dwelltimes of one condition
    :param N: number of bins to plot the histogram
    :param MAX: maximum accepted dwelltime (histogram cuts off after)
    :param cutoff_low: minimum accepted dwelltime (all dwelltimes below are ignored)
    :param cutoff_high: maximum accepted dwelltime (all dwelltimes above are ignored)
    :return: cut_dwelltimes, errors, cutofftime, MAX, bin_centres, counts
    """
    ## filtering the dwell times with low and high cutoff
    cut_dt = dwelltime[(cutoff_low < dwelltime) & (dwelltime < cutoff_high)]

    print('Total dwell times #', len(cut_dt))
    print('Dwell times #', sorted(cut_dt)[:5])
    print('Dwell times #', sorted(cut_dt)[-5:])
    print('bins:', N)
    print('Cutoffs (low, high):', cutoff_low, cutoff_high)

    samples_bootstrapped = []
    bins = 10 ** np.linspace(np.log10(np.min(cut_dt)), np.log10(np.max(cut_dt)), N)
    counts, bin_edges = np.histogram(cut_dt, bins=bins)
    bin_centres = (bin_edges[:-1] + bin_edges[1:]) / 2
    bin_width = (bin_edges[1:] - bin_edges[:-1])
    count = counts / bin_width / np.sum(counts)
    k = (1 / cut_dt.mean())

    k_bs = []
    for i in range(0, 1000):
        dwell_bs = bootstrap(cut_dt, n=None)
        counts_bs, bin_edges = np.histogram(dwell_bs, bins=bins)
        bin_width = (bin_edges[1:] - bin_edges[:-1])
        counts_bs = counts_bs / bin_width / np.sum(counts_bs)  # normalizing
        samples_bootstrapped.append(counts_bs)
        k_bs.append(1 / dwell_bs.mean())

    sd = np.std(samples_bootstrapped, axis=0)  # mean + sigma = 64 % CI, mean + 2*sigma = 95% CI
    return cut_dt, sd, bin_centres, count, k, k_bs


def plot_exp1(read_params=0):
    fnames = tkfd.askopenfilenames()
    for fname in fnames:
        print(fname)
        ## read dwell times from a tab separated text file
        ydata = np.loadtxt(fname, delimiter='\t')
        hd, tl = os.path.split(fname)
        dir_out = hd + '/out/'
        if read_params:
            if os.path.exists(f'{dir_out + tl[:-4]}_exp1_res.txt'):
                print('Fitting results exists!')
                try:
                    res = pd.read_csv(f'{dir_out + tl[:-4]}_exp1_res.txt', delimiter='\t', header=0)
                    bins = int(res['bins'])
                    cutoff_l = float(res['cutoff_l'])
                    cutoff_h = float(res['cutoff_h'])
                except:
                    res = np.loadtxt(f'{dir_out + tl[:-4]}_exp1_res.txt')
                    bins = int(res[1003])
                    cutoff_l = res[1004]
                    cutoff_h = res[1005]
                    print('Old model result found.')
            else:
                print('Unable to read parameters! Will use default parameters.')
                bins = 10
                cutoff_l = 0
                cutoff_h = 10000
        else:
            print('Using default parameters!')
            bins = 11
            cutoff_l = 0
            cutoff_h = 10000

        cut_dwelltimes, errors, bin_centres, counts, k, k_bs = create_barplot_data(ydata, N=bins, cutoff_low=cutoff_l,
                                                                                   cutoff_high=cutoff_h)

        k_sd = np.std(k_bs)

        print('Tau (s):', 1 / k)
        print('k (1/s):', k)

        x1 = np.linspace(bin_centres[0], bin_centres[-1], num=200)
        y_fitted = 1 * k * np.exp(-x1 * k)

        ## plot actual data
        fig = plt.figure(constrained_layout=True)
        ax = fig.add_subplot()
        ax.errorbar(bin_centres, counts, fmt='o', color='gray', mfc='none', lw=1, yerr=2 * errors, capsize=2, capthick=1,
                    zorder=1)  # 2*errors = 2sd = 95% CI

        # plot fitted data on original data
        ax.plot(x1, y_fitted, c='r', linestyle='dashed', lw=1.5, label="Exp1 MLE fit", zorder=2)
        # plt.legend()
        ax.set_xlabel("Dwell time (s)")
        ax.set_ylabel("Prob. dens.")
        # ax.set_xlim(1, 3000)
        ax.set_xscale('log')
        ax.set_yscale('log')

        plt.show()

        # save results
        choice = tkmsg.askquestion('Yes/No', 'Want to save?', icon='warning')
        if choice == 'yes':
            if os.path.exists(dir_out):
                fname = dir_out + tl
            else:
                os.mkdir(dir_out)
                fname = dir_out + tl

            df_res = pd.DataFrame({'k': [k], 'k_sd': k_sd, 'bins': bins, 'cutoff_l': cutoff_l, 'cutoff_h': cutoff_h})
            df_res.to_csv(f'{fname[:-4]}_exp1_res.txt', sep='\t', header=True, float_format='%.6f', index=False)
            np.savetxt(f'{fname[:-4]}_exp1_bs.txt', k_bs, fmt='%.7f', delimiter='\t')  # %f saves in normal decimal
            if len(ydata) != len(cut_dwelltimes):
                np.savetxt(f'{fname[:-4]}exp1_cut.txt', np.sort(cut_dwelltimes), fmt='%f',
                           delimiter='\t')  # %f saves in normal decimal
            fig.savefig(f'{fname[:-4]}_exp1_plot.pdf')
            # fig.savefig(f'{fname[:-4]}_exp1plot.png')


if __name__ == '__main__':
    ans = tkmsg.askquestion('Yes/No', 'Want to read params (needs previously analysed data)?', icon='warning')
    if ans == 'yes':
        plot_exp1(read_params=1)
    else:
        plot_exp1(read_params=0)
